// STL
#include <iostream>
#include <string>
#include <vector>

// HESS
#include <sashfile/HandlerC.hh>
#include <sash/DataSet.hh>
#include <sash/HESSArray.hh>
#include <sash/RunHeader.hh>
#include <sash/EventHeader.hh>
#include <sash/Telescope.hh>
#include <sash/Pixel.hh>
#include <sash/PixelConfig.hh>
#include <sash/Pointer.hh>
#include <sash/PointerSet.hh>
#include <sash/PointerSetIterator.hh>
#include <sash/IntensityData.hh>

// ROOT
#include <TCanvas.h>
#include <TH2F.h>
#include <TGraph.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

//#define DEBUG

bool is_HESSI_stereo(const Sash::EventHeader* header, Sash::HESSArray *fhess);
void fill_histograms(Sash::Pointer<Sash::Pixel> pix,
                     const Sash::IntensityData* intdata, TH2F * h1, TH2F * h2,
                     TH2F * h3);

void create_tf_input(
    std::string filename_in =
        "/lustre/fs20/group/hess/user/steppa/simulations/sim_hessarray/phase2b/gamma_for_tests_3pe/Data/HESSI_U_Magic/gamma_20deg_180deg_run801___phase2b2_hess_off0.5_dst.root",
    std::string out_filname = "test_hist.root") {

  SashFile::HandlerC *han = new SashFile::HandlerC("DST");
  han->DisableTimeFixes(true);
  if (!han->ConnectFileByName(filename_in.c_str())) {
    std::cout << "Can't find file: " << filename_in << std::endl;
    delete han;
    return;
  }

  Sash::DataSet *ds_run = han->GetDataSet("run");
  if (!ds_run) {
    std::cout << "Can't find the dataset run ! " << std::endl;
    delete han;
    return;
  }
  Sash::DataSet *ds_evt = han->GetDataSet("DST");
  if (!ds_evt) {
    std::cout << "Can't find the dataset events ! " << std::endl;
    delete han;
    return;
  }

  TFile f(out_filname.c_str(), "recreate");
  TTree* CT1 = new TTree("CT1", "CT1 Events (ASA)");
  TTree *CT2 = new TTree("CT2", "CT2 Events (ASA)");
  TTree *CT3 = new TTree("CT3", "CT3 Events (ASA)");
  TTree *CT4 = new TTree("CT4", "CT4 Events (ASA)");
//  TTree *CT5 = new TTree("CT5", "CT5 Events (ASA)");
  TH2F* h1[4];
  TH2F* h2[4];
  TH2F* h3[4];
  for (size_t telID = 1; telID < 5; telID++) {
//    std::stringstream name;
//    name << "CT" << telID;
    h1[telID - 1] = new TH2F("full camera", "full camera", 64, 0, 64, 36, 0, 36);
    h2[telID - 1] = new TH2F("camera set 1", "camera set 1", 32, 0, 32, 18, 0, 18);
    h3[telID - 1] = new TH2F("camera set 2", "camera set 2", 32, 0, 32, 18, 0, 18);
    TTree* t = 0;
    switch (telID) {
      case 1: {
        t = CT1;
        break;
      }
      case 2: {
        t = CT2;
        break;
      }
      case 3: {
        t = CT3;
        break;
      }
      case 4: {
        t = CT4;
        break;
      }
      default:
        continue;
    }
    t->Branch("h1", "TH2F", &h1[telID - 1], 32000, 0);
    t->Branch("h2", "TH2F", &h2[telID - 1], 32000, 0);
    t->Branch("h3", "TH2F", &h3[telID - 1], 32000, 0);
  }

  Sash::HESSArray *fhess = &Sash::HESSArray::GetHESSArray();
  ds_run->GetEntry(0);

  const Sash::EventHeader* header = fhess->Get<Sash::EventHeader>();
  bool stop = false;
  for (size_t evt = 0; evt < ds_evt->GetEntries() && !stop; evt++) {
    ds_evt->GetEntry(evt);

    if (!is_HESSI_stereo(header, fhess)) {

#ifdef DEBUG
      std::cout << "Event: " << evt << " is no H.E.S.S. I stereo event"
                << std::endl;
#endif //DEBUG

      continue;
    }

    for (Sash::PointerSet<Sash::Telescope>::const_iterator it_tel = header
        ->GetTelWData().begin(); it_tel != header->GetTelWData().end();
        ++it_tel) {
//      if ((*it_tel)->GetId() == 1) {
      TTree* t = 0;
      switch ((*it_tel)->GetId()) {
        case 1: {
          t = CT1;
          break;
        }
        case 2: {
          t = CT2;
          break;
        }
        case 3: {
          t = CT3;
          break;
        }
        case 4: {
          t = CT4;
          break;
        }
        default:
          continue;
      }
      h1[(*it_tel)->GetId() - 1]->Reset();
      h2[(*it_tel)->GetId() - 1]->Reset();
      h3[(*it_tel)->GetId() - 1]->Reset();

//        stop = true;
      const Sash::IntensityData* intdata = (*it_tel)->Get < Sash::IntensityData
          > ("Clean0306MT");

      Sash::Pointer<Sash::Pixel> beginpixel = (*it_tel)->beginPixel();
      Sash::Pointer<Sash::Pixel> endpixel = (*it_tel)->endPixel();

#ifdef DEBUG
      std::cout << "Event: " << evt << std::endl << "Begin: " << beginpixel
          << "\tEnd: " << endpixel << std::endl;
#endif //DEBUG

      for (Sash::Pointer<Sash::Pixel> pix = beginpixel; pix != endpixel;
          ++pix) {

        fill_histograms(pix, intdata, h1[(*it_tel)->GetId() - 1],
                        h2[(*it_tel)->GetId() - 1], h3[(*it_tel)->GetId() - 1]);
      }
      t->Fill();
//      }
    }
  }
  CT1->Print();
  CT2->Print();
  CT3->Print();
  CT4->Print();
//  T->Print();
  f.Write();
#ifdef DEBUG
  TCanvas *c1 = new TCanvas("c1", "Graph Draw Options", 200, 10, 400, 400);
  c1->cd(0);
  h1[0]->Draw("colz");
  TCanvas *c2 = new TCanvas("c2", "Graph Draw Options", 200, 10, 400, 400);
  c2->cd(0);
  h2[0]->Draw("colz");
  TCanvas *c3 = new TCanvas("c3", "Graph Draw Options", 200, 10, 400, 400);
  c3->cd(0);
  h3[0]->Draw("colz");
#endif //DEBUG
}

bool is_HESSI_stereo(const Sash::EventHeader* header, Sash::HESSArray *fhess) {
  int is_HESSI_stereo = 0;
  for (size_t telID = 1; telID < 5; telID++) {
    if (header->CheckTelWData(Sash::Pointer<Sash::Telescope>(fhess, telID)))
      is_HESSI_stereo++;
  }
  return is_HESSI_stereo > 1;
}

void fill_histograms(Sash::Pointer<Sash::Pixel> pix,
                     const Sash::IntensityData* intdata, TH2F * h1, TH2F * h2,
                     TH2F * h3) {

  double intensity = intdata->GetIntensity(pix)->fIntensity;
  double x = pix->GetConfig()->GetPos().GetX();
  double y = pix->GetConfig()->GetPos().GetY();
  double d = pix->GetConfig()->GetSize();

#ifdef DEBUG
  std::cout << "PixId: " << pix->GetConfig()->GetPixelID() << "\tPixX: : " << x
      << "\tPixY: : " << y << "\tPixInt: : " << intensity << "\n";
#endif //DEBUG

  double xs = 2 * x / d;
  double ys = 2 * y / (d * sqrt(3));
  int xr = round(xs);
  int yr = round(ys);
  if (xr < 0)
    xr++;
  if (yr < 0)
    yr++;

  xr += 31;
  yr += 17;

  h1->Fill(xr, yr, intensity + 1);

  if (yr % 2) {
    h2->Fill(((xr - (xr % 2)) / 2), ((yr - (yr % 2)) / 2), intensity + 1);
  } else {
    h3->Fill(((xr - (xr % 2)) / 2), ((yr - (yr % 2)) / 2), intensity + 1);
  }
}
