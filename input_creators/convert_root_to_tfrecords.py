from ROOT import TChain, TFile, TTree, TH2F
from root_numpy import hist2array
import numpy as np
import tensorflow as tf
from tqdm import tqdm
import os, sys, math

def _float_feature(value):
  return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))
  
def _int64_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))
  
def _bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))
  
outfile = "/lustre/fs20/group/hess/user/steppa/tf_examples/tfrecords/examples_"
list = []
for fname in os.listdir("/lustre/fs20/group/hess/user/steppa/tf_examples/root"):
    list.append("/lustre/fs20/group/hess/user/steppa/tf_examples/root/"+fname)
nlist = np.array(list, dtype=str)
np.random.shuffle(nlist)
split = 10
files = int(math.ceil(nlist.size / split))
for file_num in range(files):
    start = split*file_num
    end = start+split
    if end > nlist.size:
        end = nlist.size
    ch = TChain("events")
    for i in range(start,end):
        ch.Add(nlist[i])
    index_list = np.arange(ch.GetEntries())
    np.random.shuffle(index_list)
    
    writer = tf.python_io.TFRecordWriter(outfile+str(file_num)+".tfrecods")
    for index in tqdm(index_list) :
        ch.GetEntry(index)
        set_1 = hist2array(ch.camera_set_1)
        set_1 = set_1.astype(np.float32)
        set_2 = hist2array(ch.camera_set_2)
        set_2 = set_2.astype(np.float32)
        primaryID = ch.primaryID
        energy = ch.energy
        pointing_alt = ch.pointing_alt
        pointing_az = ch.pointing_az
        event_alt = ch.event_alt
        event_az = ch.event_az
    
        feature = {'label/primaryID': _int64_feature(primaryID),
               'label/energy': _float_feature(energy), 
               'label/event_alt': _float_feature(event_alt), 
               'label/event_az': _float_feature(event_az), 
               'feature/set_1': _bytes_feature(tf.compat.as_bytes(set_1.tostring())),
               'feature/set_2': _bytes_feature(tf.compat.as_bytes(set_2.tostring())),
               'feature/pointing_alt': _float_feature(pointing_alt), 
               'feature/pointing_az': _float_feature(pointing_az)
            }
    
        example = tf.train.Example(features=tf.train.Features(feature=feature))
        writer.write(example.SerializeToString())
    writer.close()

# f = TFile("tf_example.root")
# t = f.Get("events")
# index_list = np.arange(t.GetEntries())
# np.random.shuffle(index_list)
# writer = tf.python_io.TFRecordWriter("test.tfrecords")
# for index in tqdm(index_list) :
#     t.GetEntry(index)
#     set_1 = hist2array(t.camera_set_1)
#     set_1 = set_1.astype(np.float32)
# #     set_1_raw = set_1.flatten()
#     set_2 = hist2array(t.camera_set_2)
# #     set_2_raw = set_2.flatten()
#     primaryID = t.primaryID
#     energy = t.energy
#     pointing_alt = t.pointing_alt
#     pointing_az = t.pointing_az
#     event_alt = t.event_alt
#     event_az = t.event_az
#     
#     feature = {'label/primaryID': _int64_feature(primaryID),
#                'label/energy': _float_feature(energy), 
#                'label/event_alt': _float_feature(event_alt), 
#                'label/event_az': _float_feature(event_az), 
#                'feature/set_1': _bytes_feature(tf.compat.as_bytes(set_1.tostring())),
#                'feature/set_2': _bytes_feature(tf.compat.as_bytes(set_2.tostring())),
#                'feature/pointing_alt': _float_feature(pointing_alt), 
#                'feature/pointing_az': _float_feature(pointing_az)
#                }
#     
#     example = tf.train.Example(features=tf.train.Features(feature=feature))
# #     example = tf.train.Example(
# #         features=tf.train.Features(
# #           feature={
# #             'label': tf.train.Feature(int64_list=tf.train.Int64List(value=[label])),
# #             'set_1': _floats_feature(set_1_raw),
# #             'set_2': _floats_feature(set_2_raw),
# #             }
# #         )
# #     )
#     writer.write(example.SerializeToString())
# writer.close()
# f.Close()