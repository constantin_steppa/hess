from __future__ import print_function

import tensorflow as tf
import math

# Parameters
learning_rate = 0.001
training_iters = 500000
batch_size = 128
display_step = 10

# Network Parameters
n_input = 32 * 18  # MNIST data input (img shape: 28*28)
n_classes = 2  # MNIST total classes (0-9 digits)
dropout = 0.75  # Dropout, probability to keep units

# tf Graph input
s1 = tf.placeholder(tf.float32, [None, 32, 18, 1])
s2 = tf.placeholder(tf.float32, [None, 32, 18, 1])
y = tf.placeholder(tf.float32, [None, n_classes])
# keep_prob = tf.placeholder(tf.float32)  # dropout (keep probability)

def read(data_path):
    feature = {'feature/set_1': tf.FixedLenFeature([], tf.string),
               'feature/set_2': tf.FixedLenFeature([], tf.string),
               'label/primaryID': tf.FixedLenFeature([], tf.int64)}
    # Create a list of filenames and pass it to a queue
    filename_queue = tf.train.string_input_producer(data_path, num_epochs=1)
    # Define a reader and read the next record
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)
    # Decode the record read by the reader
    features = tf.parse_single_example(serialized_example, features=feature)
    # Convert the image data from string back to the numbers
    s1 = tf.decode_raw(features['feature/set_1'], tf.float32)
    s2 = tf.decode_raw(features['feature/set_2'], tf.float32)
    
    # Cast label data into int32
    y = tf.cast(features['label/primaryID'], tf.int32)
    # Reshape image data into the original shape
    s1 = tf.reshape(s1, [32, 18, 1])
    s2 = tf.reshape(s2, [32, 18, 1])
    
    # Any preprocessing here ...
    
    return s1, s2, y

# Create some wrappers for simplicity
def conv2d(s1, s2, w1, w2, b1, b2, strides=1):
    # Conv2D wrapper, with bias and relu activation
    s1_out_tmp_1 = tf.nn.conv2d(s1, w2, strides=[1, strides, strides, 1], padding='SAME')
    s1_out_tmp_2 = tf.nn.conv2d(s2, w1, strides=[1, strides, strides, 1], padding='SAME')
    s1_out = tf.add(s1_out_tmp_1, s1_out_tmp_2)
    s1_out = tf.nn.bias_add(s1_out, b1)
    s2_out_tmp_1 = tf.nn.conv2d(s1, w1, strides=[1, strides, strides, 1], padding='SAME')
    s2_out_tmp_2 = tf.nn.conv2d(s2, w2, strides=[1, strides, strides, 1], padding='SAME')
    s2_out = tf.add(s2_out_tmp_1, s2_out_tmp_2)
    s2_out = tf.nn.bias_add(s2_out, b2)
    return tf.nn.relu(s1_out), tf.nn.relu(s2_out)


def maxpool2d(s1, s2, k1=[2, 2], k2=[3, 1]):
    # MaxPool2D wrapper for hexagonal grid (ASA)
    tmp1 = tf.nn.max_pool(s1, ksize=[1, k2[0], k2[1], 1], strides=[1, 1, 1, 1],
                          padding='SAME')
    tmp2 = tf.nn.max_pool(s2, ksize=[1, k1[0], k1[1], 1], strides=[1, 1, 1, 1],
                          padding='SAME')
    print("pool tmp1:", tmp1)
    print("pool tmp2:", tmp2)
    tmp = tf.maximum(tmp1, tmp2)
    print("pool tmp:", tmp)
    end1 = tmp.get_shape().as_list()
    end1[1] -= 2 * (end1[1] % 2)
    end1[2] -= 2 * (end1[2] % 2)
#     e1 = end1[1].eval() % 2
#     e2 = end1[2].eval() % 2
#     add = tf.constant([0, e1, e2, 0])
#     print("e1:", e1, "e2:", e2, "add:", add)
#     end3 = tf.add(end1, -add)
    print("pool end1:", end1)
    s1_out = tf.strided_slice(tmp, [0, 0, 0, 0], end1, [1, 2, 2, 1])
    print("pool s1_out:", s1_out)
    # since counting starts at [1, 1] for set_2, end of strided slice must
    # contain even numbers for set_2 to have the same shape as set_1
#     e1 = end1[1].eval() % 2
#     e2 = end1[2].eval() % 2
#     e1 = end1[1] % 2
#     e2 = end1[2] % 2
#     end_mask = 0
#     if (end1[1] % 2) == 1:
#         end_mask += 1<<1
#     if (end1[2] % 2) == 1:
#         end_mask += 1<<2
# #     add = tf.constant([0, e1, e2, 0])
# #     print("e1:", e1, "e2:", e2)
#     print("end_mask:", end_mask)
#     end2 = end1 + [0, e1, e2, 0]
#     end2 = end1
#     end2[1]+=e1
#     end2[2]+=e2
    end2 = tmp.get_shape().as_list()
    print("pool end2:", end2)
    s2_out = tf.strided_slice(tmp, [0, 1, 1, 0], end2, [1, 2, 2, 1])
    print("pool s2_out:", s2_out)
    return s1_out, s2_out

def format_label(y):
    print("y:", y)
    y_out = tf.one_hot(y, n_classes)
    print("y_out:", y_out)
    return y_out

# Create model
def print_info(s1, s2, weights, biases, dropout):

    print("s1:", s1)
    print("s2:", s2)
    # Convolution Layer
    conv1_1, conv1_2 = conv2d(s1, s2, weights['wc1_1'], weights['wc1_2'], biases['bc1_1'], biases['bc1_2'])
    print("conv1_1:", conv1_1)
    print("conv1_2:", conv1_2)
    # Max Pooling (down-sampling)
    conv1_1, conv1_2 = maxpool2d(conv1_1, conv1_2, k1=[2, 2], k2=[3, 1])
    print("conv1_1 after pooling:", conv1_1)
    print("conv1_2 after pooling:", conv1_2)

    # Convolution Layer
    conv2_1, conv2_2 = conv2d(conv1_1, conv1_2, weights['wc2_1'], weights['wc2_2'], biases['bc2_1'], biases['bc2_2'])
    print("conv2_1:", conv2_1)
    print("conv2_2:", conv2_2)
    # Max Pooling (down-sampling)
    conv2_1, conv2_2 = maxpool2d(conv2_1, conv2_2, k1=[2, 2], k2=[3, 1])
    print("conv2_1 after pooling:", conv2_1)
    print("conv2_2 after pooling:", conv2_2)

    # Fully connected layer
    # Reshape conv2 output to fit fully connected layer input
    fc1_1 = tf.reshape(conv2_1, [-1, weights['wd1'].get_shape().as_list()[0]/2])
    fc1_2 = tf.reshape(conv2_2, [-1, weights['wd1'].get_shape().as_list()[0]/2])
    print("fc1_1:", fc1_1)
    print("fc1_2:", fc1_2)
    fc1 = tf.concat([fc1_1, fc1_2], 1)
    print("fc1:", fc1)
    fc1 = tf.add(tf.matmul(fc1, weights['wd1']), biases['bd1'])
    print("fc1 after matmul+bias:", fc1)
    fc1 = tf.nn.relu(fc1)
    # Apply Dropout
    fc1 = tf.nn.dropout(fc1, dropout)
 
    # Output, class prediction
    out = tf.add(tf.matmul(fc1, weights['out']), biases['out'])
    return out

# Store layers weight & bias
weights = {
    # hexagonal next neighbour conv, 1 input, 32 outputs
    'wc1_1': tf.Variable(tf.random_normal([2, 2, 1, 32])),
    'wc1_2': tf.Variable(tf.random_normal([3, 1, 1, 32])),
    # hexagonal next neighbour conv, 32 inputs, 64 outputs
    'wc2_1': tf.Variable(tf.random_normal([2, 2, 32, 64])),
    'wc2_2': tf.Variable(tf.random_normal([3, 1, 32, 64])),
    # fully connected, 2*8*5*64 inputs, 1024 outputs
    'wd1': tf.Variable(tf.random_normal([2 * 8 * 4 * 64, 1024])),
    # 1024 inputs, 2 outputs (class prediction)
    'out': tf.Variable(tf.random_normal([1024, n_classes]))
}

biases = {
    'bc1_1': tf.Variable(tf.random_normal([32])),
    'bc1_2': tf.Variable(tf.random_normal([32])),
    'bc2_1': tf.Variable(tf.random_normal([64])),
    'bc2_2': tf.Variable(tf.random_normal([64])),
    'bd1': tf.Variable(tf.random_normal([1024])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

# data_path = '/lustre/fs20/group/hess/user/steppa/tf_examples/tfrecords/examples_0.tfrecods'  # address to save the hdf5 file

data_path = []
for i in range(50):
    data_path += ['/lustre/fs20/group/hess/user/steppa/tf_examples/tfrecords/examples_' + str(i) + '.tfrecods']
    
print(data_path)

s1, s2, y = read(data_path=data_path)
s1_batch, s2_batch, y_batch = tf.train.shuffle_batch([s1, s2, y],
                                                         batch_size=batch_size,
                                                         capacity=300,
                                                         num_threads=2,
                                                         min_after_dequeue=10)
    
# Construct model
# pred = conv_net(s1_batch, s2_batch, weights, biases, keep_prob)
pred =  print_info(s1_batch, s2_batch, weights, biases, dropout)
pred_nodrop =  print_info(s1_batch, s2_batch, weights, biases, 1.0)
print("pred:", pred)
label = format_label(y_batch)
print("label:", label)

# Define loss and optimizer
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=label))
cost_nodrop = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred_nodrop, labels=label))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
 
# Evaluate model
correct_pred = tf.equal(tf.argmax(pred_nodrop, 1), tf.argmax(label, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Launch the graph
with tf.Session() as sess:

    # Initialize all global and local variables
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    sess.run(init_op)
    # Create a coordinator and run all QueueRunner objects
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    step = 1
    
    #Keep training until reach max iterations
    while step * batch_size < training_iters:
        # Creates batches by randomly shuffling tensors
          
        # Run optimization op (backprop)
#         sess.run(optimizer, feed_dict={keep_prob: dropout})
        sess.run(optimizer)
        if step % display_step == 0:
            # Calculate batch loss and accuracy
            loss, acc = sess.run([cost, accuracy])
            print("Iter " + str(step * batch_size) + ", Minibatch Loss= " + \
                  "{:.6f}".format(loss) + ", Training Accuracy= " + \
                  "{:.5f}".format(acc))
        step += 1
  
    print("Optimization Finished!")
  
    # Calculate accuracy for 256 mnist test images
    print("Testing Accuracy:", \
        sess.run(accuracy))

#     sess.run(print_info(s1_batch, s2_batch, weights, biases, keep_prob))
    
    coord.request_stop()
    
    # Wait for threads to stop
    coord.join(threads)
    sess.close()
