from __future__ import print_function

import tensorflow as tf
import numpy as np
import math, os

# machine directories
machine = "local"
#machine = "zeuthen"
directory = "/Users/steppa/git/hess/examples"
logdir = "/Users/steppa/git/hess/log"
if(machine == "zeuthen"):
    directory = "/lustre/fs20/group/hess/user/steppa/tf_examples/tfrecords"
    logdir = "afs/ifh.de/group/hess/scratch/user/steppa/scripts/hess/res/log"

# Parameters
learning_rate = 0.001
training_iters = 50000
batch_size = 500
display_step = 50
max_steps = training_iters / batch_size

# Network Parameters
n_input = 32 * 18  # HESS I camera image per set (ASA)
n_classes = 2  # gammas and background
dropout = 0.75  # Dropout, probability to keep units

# tf Graph input
with tf.name_scope('input'):
    s1 = tf.placeholder(tf.float32, [None, 32, 18, 1])
    s2 = tf.placeholder(tf.float32, [None, 32, 18, 1])
    y = tf.placeholder(tf.float32, [None, n_classes])
keep_prob = tf.placeholder(tf.float32)  # dropout (keep probability)

def read(data_path):
    feature = {'feature/set_1': tf.FixedLenFeature([], tf.string),
               'feature/set_2': tf.FixedLenFeature([], tf.string),
               'label/primaryID': tf.FixedLenFeature([], tf.int64)}
    # Create a list of filenames and pass it to a queue
    filename_queue = tf.train.string_input_producer(data_path, num_epochs=1)
    # Define a reader and read the next record
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)
    # Decode the record read by the reader
    features = tf.parse_single_example(serialized_example, features=feature)
    # Convert the image data from string back to the numbers
    s1 = tf.decode_raw(features['feature/set_1'], tf.float32)
    s2 = tf.decode_raw(features['feature/set_2'], tf.float32)
    
    # Cast label data into int64
    #*********work around for false input conversion*********
    tmp = features['label/primaryID'] % 100
    y = tf.cast(tmp, tf.int64)
    #********************************************************
    
    
    # Reshape image data into the original shape
    with tf.name_scope('input_reshape'):
        s1 = tf.reshape(s1, [32, 18, 1])
        s2 = tf.reshape(s2, [32, 18, 1])
    #    y_out = tf.one_hot(y, n_classes)
    #print("y:", y, "y_out:", y_out)
        
    # Any preprocessing here ...
    
    return s1, s2, y

# def format_label(y):
#     y_out = tf.one_hot(y, n_classes)
#     return y_out

def weight_variable(shape):
    """Create a weight variable with appropriate initialization."""
    shape_1 = [2, 2] + shape
    shape_2 = [3, 1] + shape
    initial_1 = tf.truncated_normal(shape_1, stddev=0.1)
    initial_2 = tf.truncated_normal(shape_2, stddev=0.1)
    return tf.Variable(initial_1), tf.Variable(initial_2)

def bias_variable(shape):
    """Create a bias variable with appropriate initialization."""
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def variable_summaries(var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('summaries'):
      mean = tf.reduce_mean(var)
      tf.summary.scalar('mean', mean)
      with tf.name_scope('stddev'):
        stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
      tf.summary.scalar('stddev', stddev)
      tf.summary.scalar('max', tf.reduce_max(var))
      tf.summary.scalar('min', tf.reduce_min(var))
      tf.summary.histogram('histogram', var)

# Create some wrappers for simplicity
def conv2d(s1, s2, w1, w2, b1, b2, strides=1):
    # Conv2D wrapper, with bias and relu activation
    with tf.name_scope('subset_convolution'):
        s1_out_tmp_1 = tf.nn.conv2d(s1, w2, strides=[1, strides, strides, 1], padding='SAME')
        s1_out_tmp_2 = tf.nn.conv2d(s2, w1, strides=[1, strides, strides, 1], padding='SAME')
        s1_out = tf.add(s1_out_tmp_1, s1_out_tmp_2)
        tf.summary.histogram('set1_x_W2', s1_out_tmp_1)
        tf.summary.histogram('set2_x_W1', s1_out_tmp_2)
        tf.summary.histogram('set_1', s1_out)
        s2_out_tmp_1 = tf.nn.conv2d(s1, w1, strides=[1, strides, strides, 1], padding='SAME')
        s2_out_tmp_2 = tf.nn.conv2d(s2, w2, strides=[1, strides, strides, 1], padding='SAME')
        s2_out = tf.add(s2_out_tmp_1, s2_out_tmp_2)
        tf.summary.histogram('set1_x_W1', s2_out_tmp_1)
        tf.summary.histogram('set2_x_W2', s2_out_tmp_2)
        tf.summary.histogram('set_2', s2_out)
#     return tf.nn.relu(s1_out), tf.nn.relu(s2_out)
    return tf.nn.bias_add(s1_out, b1), tf.nn.bias_add(s2_out, b2)


def maxpool2d(s1, s2, k1=[2, 2], k2=[3, 1]):
    # MaxPool2D wrapper for hexagonal grid (ASA)
    with tf.name_scope('pool'):
        tmp1 = tf.nn.max_pool(s1, ksize=[1, k2[0], k2[1], 1], strides=[1, 1, 1, 1],
                          padding='SAME')
        tmp2 = tf.nn.max_pool(s2, ksize=[1, k1[0], k1[1], 1], strides=[1, 1, 1, 1],
                          padding='SAME')
        tf.summary.histogram('pool_1', tmp1)
        tf.summary.histogram('pool_2', tmp2)
    with tf.name_scope('maximum_addition'):
        tmp = tf.maximum(tmp1, tmp2)
        tf.summary.histogram('maximum', tmp)
    end1 = tmp.get_shape().as_list()
    end1[1] -= 2 * (end1[1] % 2)
    end1[2] -= 2 * (end1[2] % 2)
    end2 = tmp.get_shape().as_list()
    with tf.name_scope('strided_slice'):
        s1_out = tf.strided_slice(tmp, [0, 0, 0, 0], end1, [1, 2, 2, 1])
        s2_out = tf.strided_slice(tmp, [0, 1, 1, 0], end2, [1, 2, 2, 1])
        tf.summary.histogram('set_1', s1_out)
        tf.summary.histogram('set_2', s2_out)
    return s1_out, s2_out

def nn_layer(s1, s2, input_dim, output_dim, layer_name, act=tf.nn.relu):
    """Reusable code for making a simple neural net layer.
    It does a matrix multiply, bias add, and then uses ReLU to nonlinearize.
    It also sets up name scoping so that the resultant graph is easy to read,
    and adds a number of summary ops.
    """
    # Adding a name scope ensures logical grouping of the layers in the graph.
    with tf.name_scope(layer_name):
      # This Variable will hold the state of the weights for the layer
      with tf.name_scope('weights'):
        w1, w2 = weight_variable([input_dim, output_dim])
        variable_summaries(w1)
        variable_summaries(w2)
      with tf.name_scope('biases'):
        b1 = bias_variable([output_dim])
        variable_summaries(b1)
        b2 = bias_variable([output_dim])
        variable_summaries(b2)
      with tf.name_scope('Wx_plus_b'):
        conv_1, conv_2 = conv2d(s1, s2, w1, w2, b1, b2)
        tf.summary.histogram('pre_activations_1', conv_1)
        tf.summary.histogram('pre_activations_2', conv_2)
      activations_1 = act(conv_1, name='activation_1')
      activations_2 = act(conv_2, name='activation_2')
      tf.summary.histogram('activations_1', activations_1)
      tf.summary.histogram('activations_2', activations_2)
      return activations_1, activations_2

# Create model
def model(s1, s2, weights, biases, dropout):

    print("s1 shape:", s1.get_shape().as_list())
    print("s2 shape:", s2.get_shape().as_list())
    # Convolution Layer
    conv1_1, conv1_2 = nn_layer(s1, s2, 1, 32, 'layer1')
    print("1. convolution")
    print("s1 shape:", conv1_1.get_shape().as_list())
    print("s2 shape:", conv1_2.get_shape().as_list())
    # Max Pooling (down-sampling)
    conv1_1, conv1_2 = maxpool2d(conv1_1, conv1_2)
    print("1. pooling")
    print("s1 shape:", conv1_1.get_shape().as_list())
    print("s2 shape:", conv1_2.get_shape().as_list())

    # Convolution Layer
    conv2_1, conv2_2 = nn_layer(conv1_1, conv1_2, 32, 64, 'layer2')
    print("2. convolution")
    print("s1 shape:", conv2_1.get_shape().as_list())
    print("s2 shape:", conv2_1.get_shape().as_list())
    # Max Pooling (down-sampling)
    conv2_1, conv2_2 = maxpool2d(conv2_1, conv2_2)
    print("2. pooling")
    print("s1 shape:", conv2_1.get_shape().as_list())
    print("s2 shape:", conv2_1.get_shape().as_list())

    # Fully connected layer
    # Reshape conv2 output to fit fully connected layer input
    fc1_1 = tf.reshape(conv2_1, [-1, weights['wd1'].get_shape().as_list()[0] / 2])
    fc1_2 = tf.reshape(conv2_2, [-1, weights['wd1'].get_shape().as_list()[0] / 2])
    print("Flatten")
    print("s1 shape:", fc1_1.get_shape().as_list())
    print("s2 shape:", fc1_2.get_shape().as_list())
    fc1 = tf.concat([fc1_1, fc1_2], 1)
    print("Concat")
    print("s1 shape:", fc1.get_shape().as_list())
    fc1 = tf.add(tf.matmul(fc1, weights['wd1']), biases['bd1'])
    print("1. FCN")
    print("s1 shape:", fc1.get_shape().as_list())
    fc1 = tf.nn.relu(fc1)
    # Apply Dropout
    fc1 = tf.nn.dropout(fc1, dropout)
 
    # Output, class prediction
    out = tf.add(tf.matmul(fc1, weights['out']), biases['out'])
    print("2. FCN")
    print("s1 shape:", out.get_shape().as_list())
    return out

# Store layers weight & bias
weights = {
#     # hexagonal next neighbour conv, 1 input, 32 outputs
#     'wc1_1': tf.Variable(tf.random_normal([2, 2, 1, 32])),
#     'wc1_2': tf.Variable(tf.random_normal([3, 1, 1, 32])),
#     # hexagonal next neighbour conv, 32 inputs, 64 outputs
#     'wc2_1': tf.Variable(tf.random_normal([2, 2, 32, 64])),
#     'wc2_2': tf.Variable(tf.random_normal([3, 1, 32, 64])),
    # fully connected, 2*8*4*64 inputs, 1024 outputs
    'wd1': tf.Variable(tf.random_normal([2 * 8 * 4 * 64, 1024])),
    # 1024 inputs, 2 outputs (class prediction)
    'out': tf.Variable(tf.random_normal([1024, n_classes]))
}

biases = {
#     'bc1_1': tf.Variable(tf.random_normal([32])),
#     'bc1_2': tf.Variable(tf.random_normal([32])),
#     'bc2_1': tf.Variable(tf.random_normal([64])),
#     'bc2_2': tf.Variable(tf.random_normal([64])),
    'bd1': tf.Variable(tf.random_normal([1024])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

data_path = []
# for i in range(50):
#     data_path += ['/lustre/fs20/group/hess/user/steppa/tf_examples/tfrecords/examples_' + str(i) + '.tfrecods']


for filename in os.listdir(directory):
    data_path += [directory + "/" + filename]

s1, s2, y = read(data_path=data_path)
s1_batch, s2_batch, y_batch = tf.train.shuffle_batch([s1, s2, y],
                                                         batch_size=batch_size,
                                                         capacity=800,
                                                         num_threads=2,
                                                         min_after_dequeue=100)
    
# Construct model
# pred = conv_net(s1_batch, s2_batch, weights, biases, keep_prob)
pred = model(s1_batch, s2_batch, weights, biases, keep_prob)
# pred_nodrop = model(s1_batch, s2_batch, weights, biases, 1.0)
# label = format_label(y_batch)

# Define loss and optimizer
with tf.name_scope('cross_entropy'):
    cost = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=pred, labels=y_batch))
    tf.summary.scalar('cross_entropy', cost)

with tf.name_scope('train'):
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
 
# Evaluate model   
with tf.name_scope('accuracy'):
    with tf.name_scope('correct_prediction'):
      correct_pred = tf.equal(tf.argmax(pred, 1), y_batch)
    with tf.name_scope('accuracy'):
      accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
tf.summary.scalar('accuracy', accuracy)

merged = tf.summary.merge_all()

if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
train_writer = tf.summary.FileWriter(logdir + "/train")
test_writer = tf.summary.FileWriter(logdir + "/test")
# Launch the graph
with tf.Session() as sess:
    
    train_writer.add_graph(sess.graph)

    # Initialize all global and local variables
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    sess.run(init_op)
    # Create a coordinator and run all QueueRunner objects
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    step = 1
    
    # Keep training until reach max iterations
    while step * batch_size < training_iters:
        # Creates batches by randomly shuffling tensors
          
        # Run optimization op (backprop)
#         sess.run(optimizer, feed_dict={keep_prob: dropout})
        sess.run(optimizer, feed_dict={keep_prob: dropout})
        if step % display_step == 0:
            # Calculate batch loss and accuracy
            summary, acc = sess.run([merged, accuracy], feed_dict={keep_prob: 1.})
            test_writer.add_summary(summary, step)
            print('Accuracy at step %s: %s' % (step, acc))
        else:
            if step % max_steps == max_steps - 2:
                run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                run_metadata = tf.RunMetadata()
                summary, _ = sess.run([merged, optimizer],
                                      feed_dict={keep_prob: dropout},
                              options=run_options,
                              run_metadata=run_metadata)
#                 summary, img, lbl, guess, _ = sess.run([merged, s1_batch, y_batch, pred, optimizer],
#                                       feed_dict={keep_prob: dropout},
#                               options=run_options,
#                               run_metadata=run_metadata)
                train_writer.add_run_metadata(run_metadata, 'step%03d' % step)
                train_writer.add_summary(summary, step)
                print('Adding run metadata for', step)
                
#                 img, lbl = sess.run([images, labels])
#                 img = tf.reshape(img, [batch_size, 32, 18])
#                 img = img.eval()
#                 img = img.astype(np.uint8)
#                 for j in range(6):
#                     plt.subplot(2, 3, j+1)
#                     plt.imshow(img[j, ...])
#                     if lbl[j, 0] == 1 and lbl[j, 1] == 0:
#                         title = 'gamma'
#                     elif lbl[j, 0] == 0 and lbl[j, 1] == 1:
#                         title = 'proton'
#                     else:
#                         title = 'unknown'
#                     plt.title(title)
#                 plt.show()
#                 guess = tf.argmax(guess, 1)
#                 for j in range(batch_size):
#                     print("step:", i, "guess:", guess[i].eval(), "label:", lbl[i])
		
                
            else:  # Record a summary
                summary, _ = sess.run([merged, optimizer], feed_dict={keep_prob: dropout})
                train_writer.add_summary(summary, step)
        step += 1
  
    print("Optimization Finished!")
    
  
    # Calculate accuracy for 256 mnist test images
    print("Testing Accuracy:", \
        sess.run(accuracy, feed_dict={keep_prob: 1.}))

#     sess.run(model(s1_batch, s2_batch, weights, biases, keep_prob))
    
    
    coord.request_stop()
    
    # Wait for threads to stop
    coord.join(threads)
    train_writer.close()
    test_writer.close()
    sess.close()
