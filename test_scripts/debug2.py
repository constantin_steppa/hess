import root_reader
import numpy as np
import matplotlib.pyplot as plt
import cnn
import tensorflow as tf
import math

with tf.variable_scope('input'):
    s1 = tf.placeholder(tf.float32, [None, 18, 32, 1])
    s2 = tf.placeholder(tf.float32, [None, 18, 32, 1])
    telswd = tf.placeholder(tf.int32, [None, 4])
    label = tf.placeholder(tf.float32, [None, 2])
    keep_prob = tf.placeholder(tf.float32)
    training = tf.placeholder(tf.bool)

file_list = []
# file_list.append("/Users/steppa/tf_examples/serialised_root/gamma/gamma__0.root")
file_list.append("/Users/steppa/tf_examples/serialised_root/proton/proton__0.root")
reader = root_reader.Reader(file_list)
telwd, s1, s2, y = reader.get_example_batch(batch_size=1)

model = cnn.HESS(n_classes=2)
pred, s1_conv1, s2_conv1 = model.process(telwd, s1, s2, keep_prob, training)
correct_pred = tf.equal(tf.argmax(pred, 1), y)
 
saver = tf.train.Saver()
 
with tf.Session() as sess:
    saver.restore(sess, "conv_1_3_1_wd_bn_lrd/model.ckpt")
    print("Model restored.")
    result, s1_1c, s2_1c, wd, img1, img2, label, corr = sess.run([pred, s1_conv1, s2_conv1, telwd, s1, s2, y, correct_pred], feed_dict={keep_prob: 1., training: False})
    type = label
    answer = result
    vergleich = corr
    print("Label", type, "Pred", answer, "Comp", vergleich)
     
    f0 = plt.figure(figsize=(13,6))
    img1 = np.reshape(img1, (img1.shape[0], 18, 32))
    img2 = np.reshape(img2, (img2.shape[0], 18, 32))
    for j in range(img1.shape[0]):
        plt.subplot(2, 4, j+1)
        plt.imshow(img1[j, ...])
        plt.subplot(2, 4, j+5)
        plt.imshow(img2[j, ...])
        
    s1_list = np.split(s1_1c, s1_1c.shape[3], 3)
    s2_list = np.split(s2_1c, s2_1c.shape[3], 3)
    print("List 1: %s List 2: %s" % (len(s1_list), len(s2_list)))
#     for layer in range(len(s1_list)):
    for layer in range(8):
        plt.figure()
        s1_1c = np.reshape(s1_list[layer], (s1_list[layer].shape[0], 18, 32))
        s2_1c = np.reshape(s2_list[layer], (s2_list[layer].shape[0], 18, 32))
        for j in range(s1_1c.shape[0]):
            plt.subplot(2, 4, j+1)
            plt.imshow(s1_1c[j, ...])
            plt.subplot(2, 4, j+5)
            plt.imshow(s2_1c[j, ...])
         
    plt.show()