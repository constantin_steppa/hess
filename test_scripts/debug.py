import root_reader
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

# file_list = []
# file_list.append("/Users/steppa/tf_examples/serialised_root/proton/proton__0.root")
# reader = root_reader.Reader(file_list)
# telwd, s1, s2, y = reader.get_example_batch(batch_size=1)

# s1 = np.reshape(s1, (s1.shape[0], 18, 32))
# s2 = np.reshape(s2, (s2.shape[0], 18, 32))

def maxpool2d(s1, s2, k1=[2, 2], k2=[1, 3], scope='pooling'):
    """Maxpooling for hexagonal image processing
        with array set addressing""" 
    with tf.variable_scope(scope):
        with tf.variable_scope('pool_set_1'):
            tmp1 = tf.nn.max_pool(s1, ksize=[1, k2[0], k2[1], 1], strides=[1, 1, 1, 1],
                                  padding='SAME')
        with tf.variable_scope('pool_set_2'):
            s2_left_padded = tf.pad(s2, [[0,0], [1,0], [1,0], [0,0]])
            tmp2 = tf.nn.max_pool(s2_left_padded, ksize=[1, k1[0], k1[1], 1], strides=[1, 1, 1, 1],
                                  padding='VALID')
        with tf.variable_scope('maximum_addition'):
            tmp = tf.maximum(tmp1, tmp2)
        with tf.variable_scope('strided_slice'):
            with tf.variable_scope('slice_set_1'):
                s1_out = tmp[::1, ::2, ::2, ::1]
            with tf.variable_scope('slice_set_2'):
                s2_out = tmp[::1, 1::2, 1::2, ::1]
                shape1 = s1_out.get_shape().as_list()
                shape2 = s2_out.get_shape().as_list()
                s2_out = tf.pad(s2_out, [[0, 0], [0, shape1[1]-shape2[1]], [0, shape1[2]-shape2[2]], [0, 0]])
        return s1_out, s2_out

a = [i for i in range(36)]
na = np.asarray(a, float)
na = np.reshape(a, [6, 6])
s1 = np.reshape(na[::2, ::1], [1, 3, 6, 1])
s2 = np.reshape(na[1::2, ::1], [1, 3, 6, 1])
print(s1)
print(s2)

s1 = tf.cast(s1, dtype=tf.float32)
s2 = tf.cast(s2, dtype=tf.float32)
s1p, s2p = maxpool2d(s1, s2)
with tf.Session() as sess:
    s1, s2 = sess.run([s1p, s2p])
    print(s1)
    print(s2)



# for j in range(1):
#     plt.subplot(2, 4, j+1)
#     plt.imshow(s1[j, ...])
#     plt.subplot(2, 4, j+5)
#     plt.imshow(s2[j, ...])
# plt.show()