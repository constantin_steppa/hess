from ROOT import TChain
# from root_numpy import hist2array
import numpy as np

def print_array(array):
    print("Shape:", array.shape, "Min:", array.min(), "Max:", array.max(), "Sum:", array.sum())
# 
# f = TFile("test_hist.root")
# t = f.Get("CT1")
# t.Print()
# entry = 0
# for event in t :
#     print("Entry: ", entry)
#     entry+=1
# #     canvas = TCanvas("c1", "Graph Draw Options", 200, 10, 400, 400);
# #     canvas.cd(0)
# #     event.full_camera.Draw("colz")
# #     canvas.Print("test.png")
#     print("full_camera")
#     print_array(hist2array(event.full_camera))
#     print("camera_set_1")
#     print_array(hist2array(event.camera_set_1))
#     print("camera_set_2")
#     print_array(hist2array(event.camera_set_2))
#     print("PrimaryID:", event.primaryID)
# f.Close()

ch = TChain("events")
ch.Add("proton__0.root")
for i in range(5):
    ch.GetEntry(i)
    telwd = np.array(ch.TelWdata)
#     print("telwd")
#     print(telwd)
    serialised_image = np.array(ch.serialised_images)
#     print("serialised_image")
#     print_array(serialised_image)
    
    pixels = 32 * 18
    start = 0
    stop = pixels
    for i in range(telwd.sum()):
        ct = "CT_" + str(i)
        print(ct)
        s1 = serialised_image[start:stop]
        s1 = s1.reshape((32, 18))
        print("s1")
        print(s1)
        start = stop
        stop += pixels
        s2 = serialised_image[start:stop]
        start = stop
        stop += pixels
        s2 = s2.reshape((32, 18))
        print("s2")
        print(s2)
