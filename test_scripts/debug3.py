import root_reader
import numpy as np
import matplotlib.pyplot as plt
import cnn
import tensorflow as tf
import math

def show_plot(img1, img2):
    xmax = img1.shape[2]
    ymax = img1.shape[1]
    yd = math.sqrt(3)
#     print('x shape', img1.shape[2])
#     print('s2', img2)
#     xbnds = np.array([0.0,xmax+0.5])
#     ybnds = np.array([0.0,ymax * yd + 0.5 * yd])
#     extent = [xbnds[0],xbnds[1],ybnds[0],ybnds[1]]
    color_map = plt.cm.plasma
    fig=plt.figure(figsize=(20,3))
    for index in range(img1.shape[0]):
#         max = img1.max() if img1.max() >= img2.max() else img2.max()
#         smin = img1.min() if img1.min() <= img2.min() else img2.min()
#         min = 1
#         for i in range(xmax):
#             for j in range(ymax):
#                 img1[index, j, i] += math.fabs(smin) + 0.1
#                 img2[index, j, i] += math.fabs(smin) + 0.1
#                 if img1[index, j, i] < min and img1[index, j, i] > 0:
#                     min = img1[index, j, i]
#                 if img2[index, j, i] < min and img2[index, j, i] > 0:
#                     min = img2[index, j, i]
        ax = fig.add_subplot(1,4,index+1, facecolor='black')
        x = []
        y = []
        c = []
        for i in range(xmax):
            for j in range(ymax):
#                 x.append(i)
#                 y.append(j*yd)
#                 c.append(img1[index, j, i])
#                 x.append(i + 0.5)
#                 y.append((j+0.5)*yd)
#                 c.append(img2[index, j, i])
                x.append(2*i)
                y.append(2*j)
                c.append(img1[index, j, i])
                x.append(2*i+1)
                y.append(2*j+1)
                c.append(img2[index, j, i])
#                 for pe in range(int(math.ceil(img1[index, j, i]/min))):
#                     x.append(i)
#                     y.append(j * math.sqrt(3))
#                 for pe in range(int(math.ceil(img2[index, j, i]/min))):
#                     x.append(i + 1/2)
#                     y.append((j + 1/2) * math.sqrt(3))
        x = np.array(x)
        y = np.array(y)
        image = plt.hexbin(x,y,c,cmap=color_map,gridsize=(x.max(), y.max()))
#         ax.set_xlim(xbnds)
#         ax.set_ylim(ybnds)
        ax.axis([x.min(), x.max(), y.min(), y.max()])
        plt.grid(True)
        cb = plt.colorbar(image,spacing='uniform',extend='max')
        plt.gca().invert_yaxis()
    return fig

with tf.variable_scope('input'):
    s1 = tf.placeholder(tf.float32, [None, 18, 32, 1])
    s2 = tf.placeholder(tf.float32, [None, 18, 32, 1])
    telswd = tf.placeholder(tf.int32, [None, 4])
    label = tf.placeholder(tf.float32, [None, 2])
    keep_prob = tf.placeholder(tf.float32)
    training = tf.placeholder(tf.bool)

file_list = []
file_list.append("/Users/steppa/tf_examples/gamma/gamma__0.root")
# file_list.append("/Users/steppa/tf_examples/proton/proton__0.root")
reader = root_reader.Reader(file_list)
telwd, s1, s2, y = reader.get_example_batch(batch_size=1)

model = cnn.HESS(n_classes=2)
pred, s1_conv1, s2_conv1 = model.process(telwd, s1, s2, keep_prob, training)
correct_pred = tf.equal(tf.argmax(pred, 1), y)
 
saver = tf.train.Saver()
 
with tf.Session() as sess:
    saver.restore(sess, "conv_1_3_1_wd_bn_lrd/model.ckpt")
    print("Model restored.")
    result, s1_1c, s2_1c, wd, img1, img2, label, corr = sess.run([pred, s1_conv1, s2_conv1, telwd, s1, s2, y, correct_pred], feed_dict={keep_prob: 1., training: False})
    type = label
    answer = result
    vergleich = corr
    print("Label", type, "Pred", answer, "Comp", vergleich)

    f0 = plt.figure(figsize=(12,3))
    img1 = np.reshape(img1, (img1.shape[0], 18, 32))
    img2 = np.reshape(img2, (img2.shape[0], 18, 32))
    for j in range(img1.shape[0]):
        plt.subplot(2, 4, j+1)
        plt.imshow(img1[j, ...])
        plt.subplot(2, 4, j+5)
        plt.imshow(img2[j, ...])
        
    f1 = show_plot(img1, img2)
    
    s1_list = np.split(s1_1c, s1_1c.shape[3], 3)
    s2_list = np.split(s2_1c, s2_1c.shape[3], 3)
    print("List 1: %s List 2: %s" % (len(s1_list), len(s2_list)))
     
#     for layer in range(len(s1_list)):
    for layer in range(10,30):
        s1_1c_x = np.reshape(s1_list[layer], (s1_list[layer].shape[0], s1_list[layer].shape[1], s1_list[layer].shape[2]))
        s2_1c_x = np.reshape(s2_list[layer], (s2_list[layer].shape[0], s2_list[layer].shape[1], s2_list[layer].shape[2]))
#         print('s1:', s1_1c_x)
#         print('s2:', s2_1c_x)
        show_plot(s1_1c_x, s2_1c_x)
    
    plt.show()

        

  
        
# #     f1 = plt.figure()
#     s1_list = np.split(s1_1c, s1_1c.shape[3], 3)
#     s2_list = np.split(s2_1c, s2_1c.shape[3], 3)
#     print("List 1: %s List 2: %s" % (len(s1_list), len(s2_list)))
#     
# #     for layer in range(len(s1_list)):
#     for layer in range(8):
#         plt.figure()
#         s1_1c = np.reshape(s1_list[layer], (s1_list[layer].shape[0], 18, 32))
#         s2_1c = np.reshape(s2_list[layer], (s2_list[layer].shape[0], 18, 32))
#         for j in range(s1_1c.shape[0]):
#             plt.subplot(2, 4, j+1)
#             plt.imshow(s1_1c[j, ...])
#             plt.subplot(2, 4, j+5)
#             plt.imshow(s2_1c[j, ...])
#         
#     plt.show()