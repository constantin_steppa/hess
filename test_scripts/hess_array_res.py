from __future__ import print_function

import tensorflow as tf
import numpy as np
import math, os

with tf.variable_scope('global_variables'):
    telescopes = 4
    telescope_model_output_size = 10
    n_classes = 2  # gammas and background
    batch_size = 1

# tf Graph input
with tf.variable_scope('input'):
#     telswd = tf.placeholder(tf.int32, [None, 4])
#     hess1_array = tf.placeholder(tf.float32, [None, telescopes * telescope_model_output_size])
    y = tf.placeholder(tf.int32, [None, n_classes])
    telswd = tf.placeholder(tf.int32, [None, 4])
    serialised_images = tf.placeholder(tf.float32, [None, None])
    hess1_array = tf.placeholder(tf.float32, [None, 10])

def read(data_path):
    with tf.variable_scope('read_input'):
        filename_queue = tf.train.string_input_producer(data_path, num_epochs=1)
        reader = tf.TFRecordReader()
        _, serialized_example = reader.read(filename_queue)
        feature = {'feature/telwd': tf.FixedLenFeature([], tf.string),
                   'feature/serialised_images': tf.FixedLenFeature([], tf.string),
                   'label/primaryID': tf.FixedLenFeature([], tf.int64)}
        features = tf.parse_single_example(serialized_example, features=feature)
#         telswd = tf.decode_raw(features['feature/telswd'], tf.int32)
        telswd = tf.decode_raw(features['feature/telwd'], tf.int32)
        ntelswd = tf.reduce_sum(telswd)
        telswd = tf.reshape(telswd, [telescopes])
        serialised_images = tf.decode_raw(features['feature/serialised_images'], tf.float32)
        pixels = 2 * 32 * 18
#         shape = tf.multiply(ntelswd, tf.cast(pixels, tf.int32))
        x = [(telescopes - ntelswd) * pixels,]
        zeros = tf.zeros(x, tf.float32)
        serialised_images = tf.concat(serialised_images, zeros)
#         i = tf.identity(ntelswd)
#         c = lambda i: tf.less(i, telescopes)
# #         b = lambda i: tf.concat(serialised_images, tf.zeros([pixels,], dtype=tf.int32))
#         def reduce():
#             global n
#             n -= 1
#         b = lambda i: reduce
#         tf.while_loop(c, b, [i])
        serialised_images = tf.reshape(serialised_images, npixels * telescopes)
#         with tf.variable_scope('reshape_input_arrays'):
#             s1 = tf.reshape(s1, [32, 18, 1])
#             s2 = tf.reshape(s2, [32, 18, 1])
        with tf.variable_scope('cast_label'):
            tmp = features['label/primaryID'] % 100
            y = tf.cast(tmp, tf.int64)
        return ntelswd, telswd, serialised_images, y

def make_image_batch(ntelswd_batch, serialised_images_batch):
    def decode(ntelswd, serialised_images):
        n_pixels = 32 * 18
        marker1 = []
        marker2 = []
        i = tf.constant(0)
        c = lambda i: tf.less(i, ntelswd)
        b = lambda i: marker1.append(2 * i * n_pixels), marker2.append((2 * i + 1) * n_pixels)
        tf.while_loop(c, b, [i])
        def get_set(marker):
            set = tf.slice(serialised_images, marker, [n_pixels])
            return tf.reshape(set, [1, 32, 18, 1])
        s1_list = []
        for marker in marker1:
            s1_list += [get_set(marker)]
        s2_list = []
        for marker in marker2:
            s2_list += [get_set(marker)]
        return tf.concat(s1_list), tf.concat(s2_list)
    initializer = (tf.constant(0, [None, 32, 18, 1]), tf.constant(0, [None, 32, 18, 1]))
    s1_batches, s2_batches = tf.scan(lambda _, (ntelswd, serialised_images): decode(ntelswd, serialised_images), (ntelswd_batch, serialised_images_batch), initializer)
    return tf.concat(s1_batches), tf.concat(s2_batches)

def make_event_batch(telswd_batch, tel_responses_batch):
    tel_responses_list = tf.unstack(tel_responses_batch)
    def join_tel_response(event_telswd, tel_responses_list=tel_responses_list):
        combined_tel_responses = []
        for tel in telescopes:
            mask = tf.one_hot([tel], telescopes, on_value=1, off_value=0, axis=-1, dtype=tf.int32)
            condition = tf.equal(tf.reduce_sum(tf.multiply(event_telswd, mask)), tf.cast(1, tf.int32))
            def tel_is_wd(tel_responses_list=tel_responses_list):
                return tel_responses_list.pop(0)
            def tel_not_wd():
                return tf.zeros(telescope_model_output_size)
            tel_response = tf.cond(condition, tel_is_wd, tel_not_wd)
            combined_tel_responses.append(tel_response)
        return tf.concat(combined_tel_responses)
    initializer = tf.constant(0, [1, telescopes * telescope_model_output_size])
    event_batches = tf.scan(lambda _, event_telswd: join_tel_response(event_telswd), telswd_batch, initializer)
    return tf.concat(event_batches)


data_path = []
data_path += ["/Users/steppa/git/hess/examples_0.tfrecods"]
ntelswd, telswd, serialised_images, y = read(data_path=data_path)
# ntelswd_batch, telswd_batch, serialised_images_batch, y_batch = tf.train.shuffle_batch([ntelswd, telswd, serialised_images, y],
#                                                                                          batch_size=batch_size,
#                                                                                          capacity=600,
#                                                                                          num_threads=2,
#                                                                                          min_after_dequeue=100)  
min_after_dequeue = 10
capacity = min_after_dequeue + 3 * batch_size
#   example_batch, label_batch = tf.train.shuffle_batch(
#       [example, label], batch_size=batch_size, capacity=capacity,
#       min_after_dequeue=min_after_dequeue)
ntelswd_batch, telswd_batch, serialised_images_batch, y_batch = tf.train.shuffle_batch([ntelswd, telswd, serialised_images, y],
                                                                                         batch_size=batch_size,
                                                                                         capacity=capacity,
                                                                                         num_threads=2,
                                                                                         min_after_dequeue=min_after_dequeue)  
# # ha = tf.Print(hess1_array, [hess1_array], message="This is ha: ")
# 
# image_batch = make_image_batch(ntelswd_batch, serialised_images_batch)

merged = tf.summary.merge_all()
logdir = "/Users/steppa/git/hess/res_array_log"
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
train_writer = tf.summary.FileWriter(logdir)

with tf.Session() as sess:
    train_writer.add_graph(sess.graph)
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    sess.run(init_op)
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
#     summary = sess.run(merged)
#     train_writer.add_summary(summary, 1)
# with tf.Session() as sess:
# sess = tf.InteractiveSession()
# init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
# sess.run(init_op)
# # for i in range(5):
    ntelswd_batch, telswd_batch, serialised_images_batch, y_batch = sess.run([ntelswd_batch, telswd_batch, serialised_images_batch, y_batch])
    print(ntelswd_batch, telswd_batch, serialised_images_batch, y_batch)
#     ntelswd, telswd, serialised_images, y = sess.run([ntelswd, telswd, serialised_images, y])
#     print(ntelswd, telswd, serialised_images.shape, y)

#     image_batch = sess.run(image_batch)
#     list = tf.unstack(batch)
#     for item in list:
#         
#         print(item.eval())
    
coord.request_stop()
coord.join(threads)
train_writer.close()
sess.close()
