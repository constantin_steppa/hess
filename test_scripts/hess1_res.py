from __future__ import print_function

import tensorflow as tf
import numpy as np
import math, os

# machine directories
machine = "local"
# machine = "zeuthen"
directory = "/Users/steppa/git/hess/examples"
logdir = "/Users/steppa/git/hess/res/log"
if(machine == "zeuthen"):
    directory = "/lustre/fs20/group/hess/user/steppa/tf_examples/tfrecords"
    logdir = "/afs/ifh.de/group/hess/scratch/user/steppa/scripts/hess/res/log"

# Parameters
learning_rate = 1e-4
training_iters = 30000
batch_size = 300
display_step = 5
max_steps = training_iters / batch_size

# Network Parameters
n_input = 32 * 18  # HESS I camera image per set (ASA)
n_classes = 2  # gammas and background
dropout = 0.75  # Dropout, probability to keep units

# tf Graph input
with tf.variable_scope('input'):
    s1 = tf.placeholder(tf.float32, [None, 32, 18, 1])
    s2 = tf.placeholder(tf.float32, [None, 32, 18, 1])
    y = tf.placeholder(tf.float32, [None, n_classes])
keep_prob = tf.placeholder(tf.float32)
training = tf.placeholder(tf.bool)

def read(data_path):
    with tf.variable_scope('read_input'):
        filename_queue = tf.train.string_input_producer(data_path, num_epochs=1)
        reader = tf.TFRecordReader()
        _, serialized_example = reader.read(filename_queue)
        feature = {'feature/set_1': tf.FixedLenFeature([], tf.string),
                   'feature/set_2': tf.FixedLenFeature([], tf.string),
                   'label/primaryID': tf.FixedLenFeature([], tf.int64)}
        features = tf.parse_single_example(serialized_example, features=feature)
        s1 = tf.decode_raw(features['feature/set_1'], tf.float32)
        s2 = tf.decode_raw(features['feature/set_2'], tf.float32)
        with tf.variable_scope('reshape_input_arrays'):
            s1 = tf.reshape(s1, [32, 18, 1])
            s2 = tf.reshape(s2, [32, 18, 1])
        with tf.variable_scope('cast_label'):
            tmp = features['label/primaryID'] % 100
            y = tf.cast(tmp, tf.int64)
        return s1, s2, y

def weight_variable(shape):
    shape_1 = [2, 2] + shape
    shape_2 = [3, 1] + shape
    initial_1 = tf.truncated_normal(shape_1, stddev=0.1)
    initial_2 = tf.truncated_normal(shape_2, stddev=0.1)
    return tf.Variable(initial_1), tf.Variable(initial_2)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def variable_summaries(var):
    with tf.variable_scope('summaries'):
      mean = tf.reduce_mean(var)
      tf.summary.scalar('mean', mean)
      with tf.variable_scope('stddev'):
        stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
      tf.summary.scalar('stddev', stddev)
      tf.summary.scalar('max', tf.reduce_max(var))
      tf.summary.scalar('min', tf.reduce_min(var))
      tf.summary.histogram('histogram', var)
      
def conv2d(s1, s2, input_dim, output_dim, strides=1, scope='convolution'):
    with tf.variable_scope(scope):
        with tf.variable_scope('weights'):
            w1, w2 = weight_variable([input_dim, output_dim])
            variable_summaries(w1)
            variable_summaries(w2)
#             tf.add_to_collection('weights', w1)
#             tf.add_to_collection('weights', w2)
        with tf.variable_scope('biases'):
            b1 = bias_variable([output_dim])
            variable_summaries(b1)
#             tf.add_to_collection('biases', b1)
            b2 = bias_variable([output_dim])
            variable_summaries(b2)
#             tf.add_to_collection('biases', b2)
        with tf.variable_scope('subset_convolution'):
            with tf.variable_scope('convolve_set_1'):
                s1_out_tmp_1 = tf.nn.conv2d(s1, w2, strides=[1, strides, strides, 1], padding='SAME')
                s1_out_tmp_2 = tf.nn.conv2d(s2, w1, strides=[1, strides, strides, 1], padding='SAME')
                s1_out = tf.add(s1_out_tmp_1, s1_out_tmp_2)
            with tf.variable_scope('convolve_set_2'):
                s2_out_tmp_1 = tf.nn.conv2d(s1, w1, strides=[1, strides, strides, 1], padding='SAME')
                s2_out_tmp_2 = tf.nn.conv2d(s2, w2, strides=[1, strides, strides, 1], padding='SAME')
                s2_out = tf.add(s2_out_tmp_1, s2_out_tmp_2)
        return tf.nn.bias_add(s1_out, b1), tf.nn.bias_add(s2_out, b2)


def maxpool2d(s1, s2, k1=[2, 2], k2=[3, 1], scope='pooling'):
    with tf.variable_scope(scope):
        with tf.variable_scope('pool_set_1'):
            tmp1 = tf.nn.max_pool(s1, ksize=[1, k2[0], k2[1], 1], strides=[1, 1, 1, 1],
                                  padding='SAME')
        with tf.variable_scope('pool_set_2'):
            tmp2 = tf.nn.max_pool(s2, ksize=[1, k1[0], k1[1], 1], strides=[1, 1, 1, 1],
                                  padding='SAME')
        with tf.variable_scope('maximum_addition'):
            tmp = tf.maximum(tmp1, tmp2)
        with tf.variable_scope('strided_slice'):
            with tf.variable_scope('slice_set_1'):
                end1 = tmp.get_shape().as_list()
                end1[1] -= 2 * (end1[1] % 2)
                end1[2] -= 2 * (end1[2] % 2)
                s1_out = tf.strided_slice(tmp, [0, 0, 0, 0], end1, [1, 2, 2, 1])
            with tf.variable_scope('slice_set_2'):
                end2 = tmp.get_shape().as_list()
                s2_out = tf.strided_slice(tmp, [0, 1, 1, 0], end2, [1, 2, 2, 1])
        return s1_out, s2_out

def batch_norm(x, n_out, phase_train, scope='batch_norm', affine=True):
  with tf.variable_scope(scope):
    beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
      name='beta', trainable=True)
    gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
      name='gamma', trainable=True)

    batch_mean, batch_var = tf.nn.moments(x, [0, 1, 2], name='moments')
    ema = tf.train.ExponentialMovingAverage(decay=0.99)

    def mean_var_with_update():
      ema_apply_op = ema.apply([batch_mean, batch_var])
      with tf.control_dependencies([ema_apply_op]):
        return tf.identity(batch_mean), tf.identity(batch_var)
    
    mean, var = tf.cond(phase_train,
      mean_var_with_update,
      lambda: (ema.average(batch_mean), ema.average(batch_var)))

    normed = tf.nn.batch_norm_with_global_normalization(x, mean, var,
      beta, gamma, 1e-3, affine)
  return normed
  
def residual_block(s1, s2, n_in, n_out, subsample, phase_train, scope='res_block'):
  with tf.variable_scope(scope):
    if subsample:
      y1, y2 = conv2d(s1, s2, n_in, n_out)
      shortcut1, shortcut2 = conv2d(s1, s2, n_in, n_out)
    else:
      y1, y2 = conv2d(s1, s2, n_in, n_out)
      shortcut1 = tf.identity(s1, name='shortcut1')
      shortcut2 = tf.identity(s2, name='shortcut2')
    y1 = batch_norm(y1, n_out, phase_train, scope='batch_norm_1')
    y2 = batch_norm(y2, n_out, phase_train, scope='batch_norm_1')
    y1 = tf.nn.relu(y1, name='relu_1')
    y2 = tf.nn.relu(y2, name='relu_1')
    tf.summary.histogram('activations1_1', y1)
    tf.summary.histogram('activations1_2', y2)
    y1, y2 = conv2d(y1, y2, n_out, n_out)
    y1 = batch_norm(y1, n_out, phase_train, scope='batch_norm_2')
    y2 = batch_norm(y2, n_out, phase_train, scope='batch_norm_2')
    y1 = tf.add(y1, shortcut1)
    y2 = tf.add(y2, shortcut2)
    y1 = tf.nn.relu(y1, name='relu_2')
    y2 = tf.nn.relu(y2, name='relu_2')
    tf.summary.histogram('activations2_1', y1)
    tf.summary.histogram('activations2_2', y2)
  return y1, y2 

def residual_group(s1, s2, n_in, n_out, n, first_subsample, phase_train, scope='res_group'):
  with tf.variable_scope(scope):
    y1, y2 = residual_block(s1, s2, n_in, n_out, first_subsample, phase_train, scope='res_block_1')
    for i in xrange(n - 1):
      y1, y2 = residual_block(y1, y2, n_out, n_out, False, phase_train, scope='res_block_%d' % (i + 2))
  return y1, y2

def model(s1, s2, n, n_classes, dropout, phase_train, scope='res_net'):
  with tf.variable_scope(scope):
      
    print("s1 shape:", s1.get_shape().as_list())
    print("s2 shape:", s2.get_shape().as_list())
    
    with tf.variable_scope('convolution_1'):  
        y1, y2 = conv2d(s1, s2, 1, 16)
        y1 = batch_norm(y1, 16, phase_train, scope='batch_norm_init_s1')
        y2 = batch_norm(y2, 16, phase_train, scope='batch_norm_init_s2')
        y1 = tf.nn.relu(y1, name='relu_init_s1')
        y2 = tf.nn.relu(y2, name='relu_init_s2')
        print("1. convolution")
        print("s1 shape:", y1.get_shape().as_list())
        print("s2 shape:", y2.get_shape().as_list())
    
    y1, y2 = residual_group(y1, y2, 16, 16, n, False, phase_train, scope='res_group_1')
    print("1. residual")
    print("s1 shape:", y1.get_shape().as_list())
    print("s2 shape:", y2.get_shape().as_list())
    
    y1, y2 = maxpool2d(y1, y2, scope='pool_1')
    print("1. pooling")
    print("s1 shape:", y1.get_shape().as_list())
    print("s2 shape:", y2.get_shape().as_list())
    
    y1, y2 = residual_group(y1, y2, 16, 32, n, True, phase_train, scope='res_group_2')
    y1, y2 = residual_group(y1, y2, 32, 64, n, True, phase_train, scope='res_group_3')
    print("2. residual")
    print("s1 shape:", y1.get_shape().as_list())
    print("s2 shape:", y2.get_shape().as_list())
    
    y1, y2 = maxpool2d(y1, y2, scope='pool_2')
    print("2. pooling")
    print("s1 shape:", y1.get_shape().as_list())
    print("s2 shape:", y2.get_shape().as_list())

    with tf.variable_scope('fc_layer_1'):
        with tf.variable_scope('weights'):
            w_fcn = tf.Variable(tf.random_normal([2 * 8 * 4 * 64, 1024]))
        with tf.variable_scope('biases'):
            b_fcn = tf.Variable(tf.random_normal([1024]))
        with tf.variable_scope('flatten'):
            fc1_1 = tf.reshape(y1, [-1, w_fcn.get_shape().as_list()[0] / 2])
            fc1_2 = tf.reshape(y2, [-1, w_fcn.get_shape().as_list()[0] / 2])
            print("Flatten")
            print("s1 shape:", fc1_1.get_shape().as_list())
            print("s2 shape:", fc1_2.get_shape().as_list())
        with tf.variable_scope('concatenate'):
            fc1 = tf.concat([fc1_1, fc1_2], 1)
            print("Concat")
            print("s1 shape:", fc1.get_shape().as_list())
    
        fc1 = tf.add(tf.matmul(fc1, w_fcn), b_fcn)
        fc1 = tf.nn.relu(fc1)
        print("1. FCN")
        print("s1 shape:", fc1.get_shape().as_list())
    
    with tf.variable_scope('fc_layer_2'):
        with tf.variable_scope('weights'):
            w_out = tf.Variable(tf.random_normal([1024, n_classes]))
        with tf.variable_scope('biases'):
            b_out = tf.Variable(tf.random_normal([n_classes]))
        with tf.variable_scope('dropout'):
            fc1 = tf.nn.dropout(fc1, dropout)
        out = tf.add(tf.matmul(fc1, w_out), b_out)
        print("2. FCN")
        print("s1 shape:", out.get_shape().as_list())
    
    return out

data_path = []
for filename in os.listdir(directory):
    data_path += [directory + "/" + filename]

s1, s2, y = read(data_path=data_path)
s1_batch, s2_batch, y_batch = tf.train.shuffle_batch([s1, s2, y],
                                                         batch_size=batch_size,
                                                         capacity=600,
                                                         num_threads=2,
                                                         min_after_dequeue=100)  
pred = model(s1_batch, s2_batch, 2, n_classes, keep_prob, training)
with tf.variable_scope('cross_entropy'):
    cost = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=pred, labels=y_batch))
    tf.summary.scalar('cross_entropy', cost)
with tf.variable_scope('train'):
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
with tf.variable_scope('accuracy'):
    with tf.variable_scope('correct_prediction'):
      correct_pred = tf.equal(tf.argmax(pred, 1), y_batch)
      accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
tf.summary.scalar('accuracy', accuracy)

merged = tf.summary.merge_all()

if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
train_writer = tf.summary.FileWriter(logdir + "/train")
test_writer = tf.summary.FileWriter(logdir + "/test")
saver = tf.train.Saver()

with tf.Session() as sess:
    train_writer.add_graph(sess.graph)
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    sess.run(init_op)

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    step = 1
    
    while step * batch_size < training_iters:
        sess.run(optimizer, feed_dict={keep_prob: dropout, training: True})
        if step % display_step == 0:
            summary, acc, label_batch = sess.run([merged, accuracy, y_batch], feed_dict={keep_prob: 1., training: False})
            test_writer.add_summary(summary, step)
            label_list = tf.unstack(label_batch)
            y = 0.
            for label in label_list:
                y += label.eval()
            y = y / batch_size
            print('At step %s: Accuracy %s with %s protons' % (step, acc, y))
        else:
            if step % max_steps == max_steps - 2:
                run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                run_metadata = tf.RunMetadata()
                summary, _ = sess.run([merged, optimizer],
                                      feed_dict={keep_prob: dropout, training: True},
                              options=run_options,
                              run_metadata=run_metadata)
                train_writer.add_run_metadata(run_metadata, 'step%03d' % step)
                train_writer.add_summary(summary, step)
                print('Adding run metadata for', step) 
            else:
                summary, _ = sess.run([merged, optimizer], feed_dict={keep_prob: dropout, training: True})
                train_writer.add_summary(summary, step)
        step += 1
  
    print("Optimization Finished!")
    
    print("Testing Accuracy:", \
        sess.run(accuracy, feed_dict={keep_prob: 1., training: False}))

    coord.request_stop()
    coord.join(threads)
    train_writer.close()
    test_writer.close()
    save_path = saver.save(sess, "/tmp/model.ckpt")
    print("Model saved in file: %s" % save_path)
    sess.close()
