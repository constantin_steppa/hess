from ROOT import TChain, TFile, TTree, TH2F
# from root_numpy import hist2array
import numpy as np
import tensorflow as tf
from tqdm import tqdm
import os, sys, math

def _float_feature(value):
  return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))
  
def _int64_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))
  
def _bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))
  
outfile = "/Users/steppa/git/hess/examples_"
list = []
list.append("/Users/steppa/git/hess/proton__0.root")
nlist = np.array(list, dtype=str)
np.random.shuffle(nlist)
files = 1
for file_num in range(files):
    start = file_num
    end = files
    if end > nlist.size:
        end = nlist.size
    ch = TChain("events")
    for i in range(start,end):
        ch.Add(nlist[i])
    index_list = np.arange(ch.GetEntries())
    np.random.shuffle(index_list)
    
    writer = tf.python_io.TFRecordWriter(outfile+str(file_num)+".tfrecods")
    for index in tqdm(index_list) :
        ch.GetEntry(index)
        telwd = np.array(ch.TelWdata)
        telwd = telwd.astype(np.int32)
        serialised_images = np.array(ch.serialised_images)
        serialised_images = serialised_images.astype(np.float32)
        primaryID = ch.primaryID
        energy = ch.energy
        pointing_alt = ch.pointing_alt
        pointing_az = ch.pointing_az
        event_alt = ch.event_alt
        event_az = ch.event_az
    
        feature = {'label/primaryID': _int64_feature(primaryID),
               'label/energy': _float_feature(energy), 
               'label/event_alt': _float_feature(event_alt), 
               'label/event_az': _float_feature(event_az), 
               'feature/telwd': _bytes_feature(tf.compat.as_bytes(telwd.tostring())),
               'feature/serialised_images': _bytes_feature(tf.compat.as_bytes(serialised_images.tostring())),
               'feature/pointing_alt': _float_feature(pointing_alt), 
               'feature/pointing_az': _float_feature(pointing_az)
            }
    
        example = tf.train.Example(features=tf.train.Features(feature=feature))
        writer.write(example.SerializeToString())
    writer.close()

