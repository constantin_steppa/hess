# import tensorflow as tf
# 
# filename = "test.tfrecords"
# for serialized_example in tf.python_io.tf_record_iterator(filename):
#     example = tf.train.Example()
#     example.ParseFromString(serialized_example)
#     
#     features = tf.parse_single_example(
#         example, features={
#             'label': tf.FixedLenFeature([], tf.string),
#             'set_1': tf.FixedLenFeature([], tf.string),
#             'set_2': tf.FixedLenFeature([], tf.string),
#         }
#     )
# #     set_1 = tf.cast(features['set_1'].values, dtype=tf.float32)
#     set_1 = tf.decode_raw(features['set_1'], tf.float32)
#     print(set_1.get_shape())
#     set_1.set_shape(18*32)
#     set_1 = tf.reshape(set_1, (18,32))
#
#     # traverse the Example format to get data
#     set_1 = example.features.feature['set_1'].float_list.value
#     set_2 = example.features.feature['set_2'].float_list.value
#     label = example.features.feature['label'].int64_list.value[0]
#     # do something
#     print set_1

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

data_path = '/lustre/fs20/group/hess/user/steppa/tf_examples/tfrecords/examples_0.tfrecods'  # address to save the hdf5 file
with tf.Session() as sess:
    feature = {'feature/set_1': tf.FixedLenFeature([], tf.string),
               'label/primaryID': tf.FixedLenFeature([], tf.int64)}
    # Create a list of filenames and pass it to a queue
    filename_queue = tf.train.string_input_producer([data_path], num_epochs=1)
    # Define a reader and read the next record
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)
    # Decode the record read by the reader
    features = tf.parse_single_example(serialized_example, features=feature)
    # Convert the image data from string back to the numbers
    image = tf.decode_raw(features['feature/set_1'], tf.float32)
    
    # Cast label data into int32
    label = tf.cast(features['label/primaryID'], tf.int32)
    # Reshape image data into the original shape
    image = tf.reshape(image, [32, 18])
    
    # Any preprocessing here ...
    
    # Creates batches by randomly shuffling tensors
    images, labels = tf.train.shuffle_batch([image, label], batch_size=10, capacity=30, num_threads=1, min_after_dequeue=10)
    
    # Initialize all global and local variables
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    sess.run(init_op)
    # Create a coordinator and run all QueueRunner objects
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    for batch_index in range(5):
        img, lbl = sess.run([images, labels])
        img = img.astype(np.uint8)
        for j in range(6):
            plt.subplot(2, 3, j+1)
            plt.imshow(img[j, ...])
            plt.title('gamma' if lbl[j]==0 else 'proton')
        plt.show()
    # Stop the threads
    coord.request_stop()
    
    # Wait for threads to stop
    coord.join(threads)
    sess.close()