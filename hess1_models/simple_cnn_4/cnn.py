import tensorflow as tf

class HESS:
    def __init__(self, n_classes=2):
        self.hess1_tels = 4
        self.telescope_model_output_size = n_classes
        self.n_classes = n_classes

    def weight_variable(self, shape, name="filter"):
        """Next neighbour filter for hexagonal image processing
            with array set addressing.
            Variable": shape = (input_dim, output_dim)""" 
        shape_1 = [2, 2] + shape
        shape_2 = [1, 3] + shape
        initial_1 = tf.truncated_normal(shape_1, stddev=0.1)
        initial_2 = tf.truncated_normal(shape_2, stddev=0.1)
        n1 = name + "_s1"
        n2 = name + "_s2"
        return tf.Variable(initial_1, name=n1), tf.Variable(initial_2, name=n2)
    
    def bias_variable(self, shape, name="bias"):
        """Variable: shape = (input_dim, output_dim)"""
        initial = tf.constant(0.1, shape=shape)
        return tf.Variable(initial, name=name)
    
    def fc_weight_variable(self, in_dim, out_dim, name="fc_weight"):
        return tf.Variable(tf.random_normal([in_dim, out_dim]), name=name)
    
    def fc_bias_variable(self, out_dim, name="fc_bias"):
        return tf.Variable(tf.constant(0.1, shape=[out_dim]), name=name)
    
    def variable_summaries(self, var):
        """Summaries to evaluate filters with Tensorboard"""
        with tf.variable_scope('summaries'):
          mean = tf.reduce_mean(var)
          tf.summary.scalar('mean', mean)
          with tf.variable_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
          tf.summary.scalar('stddev', stddev)
          tf.summary.scalar('max', tf.reduce_max(var))
          tf.summary.scalar('min', tf.reduce_min(var))
          tf.summary.histogram('histogram', var)
          
    def conv2d(self, s1, s2, input_dim, output_dim, strides=1, scope='convolution'):
        """2-D convolution for hexagonal image processing
            with array set addressing""" 
        with tf.variable_scope(scope):
            with tf.variable_scope('weights'):
                w1, w2 = self.weight_variable([input_dim, output_dim], scope+"/filter")
                self.variable_summaries(w1)
                self.variable_summaries(w2)
            with tf.variable_scope('biases'):
                b1 = self.bias_variable([output_dim], scope+"/bias_s1")
                self.variable_summaries(b1)
                b2 = self.bias_variable([output_dim], scope+"/bias_s2")
                self.variable_summaries(b2)
            with tf.variable_scope('subset_convolution'):
                with tf.variable_scope('convolve_set_1'):
                    s1_out_tmp_1 = tf.nn.conv2d(s1, w2, strides=[1, strides, strides, 1], padding='SAME')
                    s2_left_padded = tf.pad(s2, [[0,0], [1,0], [1,0], [0,0]])
                    s1_out_tmp_2 = tf.nn.conv2d(s2_left_padded, w1, strides=[1, strides, strides, 1], padding='VALID')
                    s1_out = tf.add(s1_out_tmp_1, s1_out_tmp_2)
                with tf.variable_scope('convolve_set_2'):
                    s2_out_tmp_1 = tf.nn.conv2d(s1, w1, strides=[1, strides, strides, 1], padding='SAME')
                    s2_out_tmp_2 = tf.nn.conv2d(s2, w2, strides=[1, strides, strides, 1], padding='SAME')
                    s2_out = tf.add(s2_out_tmp_1, s2_out_tmp_2)
            return tf.nn.bias_add(s1_out, b1), tf.nn.bias_add(s2_out, b2)
    
    
    def maxpool2d(self, s1, s2, k1=[2, 2], k2=[1, 3], scope='pooling'):
        """Maxpooling for hexagonal image processing
            with array set addressing""" 
        with tf.variable_scope(scope):
            with tf.variable_scope('pool_set_1'):
                tmp1 = tf.nn.max_pool(s1, ksize=[1, k2[0], k2[1], 1], strides=[1, 1, 1, 1],
                                      padding='SAME')
            with tf.variable_scope('pool_set_2'):
                tmp2 = tf.nn.max_pool(s2, ksize=[1, k1[0], k1[1], 1], strides=[1, 1, 1, 1],
                                      padding='SAME')
            with tf.variable_scope('maximum_addition'):
                tmp = tf.maximum(tmp1, tmp2)
            with tf.variable_scope('strided_slice'):
                with tf.variable_scope('slice_set_1'):
                    s1_out = tf.strided_slice(tmp, [0, 0, 0, 0], tmp.get_shape().as_list(), [1, 2, 2, 1])
                with tf.variable_scope('slice_set_2'):
                    s2_out = tf.strided_slice(tmp, [0, 1, 1, 0], tmp.get_shape().as_list(), [1, 2, 2, 1])
                    shape1 = s1_out.get_shape().as_list()
                    shape2 = s2_out.get_shape().as_list()
                    s2_out = tf.pad(s2_out, [[0, 0], [0, shape1[1]-shape2[1]], [0, shape1[2]-shape2[2]], [0, 0]])
            return s1_out, s2_out
    
    def batch_norm(self, x, n_out, phase_train, scope='batch_norm', affine=True):
        """Normalising batch with global normalisation"""
        with tf.variable_scope(scope):
            beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
              name='beta', trainable=True)
            gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
              name='gamma', trainable=True)
            batch_mean, batch_var = tf.nn.moments(x, [0, 1, 2], name='moments')
            ema = tf.train.ExponentialMovingAverage(decay=0.99)
            def mean_var_with_update():
              ema_apply_op = ema.apply([batch_mean, batch_var])
              with tf.control_dependencies([ema_apply_op]):
                return tf.identity(batch_mean), tf.identity(batch_var)
            mean, var = tf.cond(phase_train,
              mean_var_with_update,
              lambda: (ema.average(batch_mean), ema.average(batch_var)))
            normed = tf.nn.batch_norm_with_global_normalization(x, mean, var,
              beta, gamma, 1e-3, affine)
        return normed
    
    def telescope_model(self, s1, s2, dropout, phase_train):
        with tf.variable_scope("tel_model"):
            print("s1 shape:", s1.get_shape().as_list())
            print("s2 shape:", s2.get_shape().as_list())
            s1_norm0 = self.batch_norm(s1, 1, phase_train, scope='batch_norm_s1_0')
            s2_norm0 = self.batch_norm(s2, 1, phase_train, scope='batch_norm_s2_0')
            s1_conv1, s2_conv1 = self.conv2d(s1_norm0, s2_norm0, 1, 64, scope='convolution_1')
            s1_norm1 = self.batch_norm(s1_conv1, 64, phase_train, scope='batch_norm_s1_1')
            s2_norm1 = self.batch_norm(s2_conv1, 64, phase_train, scope='batch_norm_s2_1')
            s1_relu1 = tf.nn.relu(s1_norm1, name='relu_s1_1')
            s2_relu1 = tf.nn.relu(s2_norm1, name='relu_s2_1')
            print("1. convolution")
            print("s1 shape:", s1_relu1.get_shape().as_list())
            print("s2 shape:", s2_relu1.get_shape().as_list())
            s1_max1, s2_max1 = self.maxpool2d(s1_relu1, s2_relu1, scope='pool_1')
            print("1. pooling")
            print("s1 shape:", s1_max1.get_shape().as_list())
            print("s2 shape:", s2_max1.get_shape().as_list())
            
            s1_conv2, s2_conv2 = self.conv2d(s1_max1, s2_max1, 64, 128, scope='convolution_2')
            s1_norm2 = self.batch_norm(s1_conv2, 128, phase_train, scope='batch_norm_s1_2')
            s2_norm2 = self.batch_norm(s2_conv2, 128, phase_train, scope='batch_norm_s2_2')
            s1_relu2 = tf.nn.relu(s1_norm2, name='relu_s1_2')
            s2_relu2 = tf.nn.relu(s2_norm2, name='relu_s2_2')
            print("2. convolution")
            print("s1 shape:", s1_relu2.get_shape().as_list())
            print("s2 shape:", s2_relu2.get_shape().as_list())
            s1_max2, s2_max2 = self.maxpool2d(s1_relu2, s2_relu2, scope='pool_2')
            print("2. pooling")
            print("s1 shape:", s1_max2.get_shape().as_list())
            print("s2 shape:", s2_max2.get_shape().as_list())
            
            s1_conv3, s2_conv3 = self.conv2d(s1_max2, s2_max2, 128, 256, scope='convolution_3')
            s1_norm3 = self.batch_norm(s1_conv3, 256, phase_train, scope='batch_norm_s1_3')
            s2_norm3 = self.batch_norm(s2_conv3, 256, phase_train, scope='batch_norm_s2_3')
            s1_relu3 = tf.nn.relu(s1_norm3, name='relu_s1_3')
            s2_relu3 = tf.nn.relu(s2_norm3, name='relu_s2_3')
            print("3. convolution")
            print("s1 shape:", s1_relu3.get_shape().as_list())
            print("s2 shape:", s2_relu3.get_shape().as_list())
            s1_max3, s2_max3 = self.maxpool2d(s1_relu3, s2_relu3, scope='pool_3')
            print("3. pooling")
            print("s1 shape:", s1_max3.get_shape().as_list())
            print("s2 shape:", s2_max3.get_shape().as_list())
                    
            with tf.variable_scope('fc_layer_1'):
                w_fcn = self.fc_weight_variable(2 * 3 * 4 * 256, 1024, "fc1_weight")
                self.variable_summaries(w_fcn)
                b_fcn = self.fc_bias_variable(1024, "fc1_bias")
                self.variable_summaries(b_fcn)
                s1_flat = tf.reshape(s1_max3, [-1, w_fcn.get_shape().as_list()[0] / 2])
                s2_flat = tf.reshape(s2_max3, [-1, w_fcn.get_shape().as_list()[0] / 2])
                print("Flatten")
                print("s1 shape:", s1_flat.get_shape().as_list())
                print("s2 shape:", s2_flat.get_shape().as_list())
                s_concat = tf.concat([s1_flat, s2_flat], 1)
                print("Concat")
                print("s1 shape:", s_concat.get_shape().as_list())
                fc1 = tf.add(tf.matmul(s_concat, w_fcn), b_fcn)
                fc1_relu = tf.nn.relu(fc1)
                print("1. FCN")
                print("s1 shape:", fc1_relu.get_shape().as_list())
                fc1_drop = tf.nn.dropout(fc1_relu, dropout)
            with tf.variable_scope('fc_layer_2'):
                w_out = self.fc_weight_variable(1024, self.telescope_model_output_size, "fc2_weight")
                self.variable_summaries(w_out)
                b_out = self.fc_bias_variable(self.telescope_model_output_size, "fc2_bias")
                self.variable_summaries(b_out)
                out = tf.add(tf.matmul(fc1_drop, w_out), b_out)
                print("2. FCN")
                print("s1 shape:", out.get_shape().as_list())
        return out

    
    def process(self, telwd, s1, s2, dropout, phase_train):
        return self.telescope_model(s1, s2, dropout, phase_train)