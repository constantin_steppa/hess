import tensorflow as tf

class HESS:
    def __init__(self, batch_size, n_classes=2):
        self.hess1_tels = 4
        self.telescope_model_output_size = 2
        self.n_classes = n_classes
        self.batch_size = batch_size

    def weight_variable(self, shape, name="filter"):
        """Next neighbour filter for hexagonal image processing
            with array set addressing.
            Variable": shape = (input_dim, output_dim)""" 
        shape_1 = [2, 2] + shape
        shape_2 = [1, 3] + shape
        initial_1 = tf.truncated_normal(shape_1, stddev=0.1)
        initial_2 = tf.truncated_normal(shape_2, stddev=0.1)
        n1 = name + "_s1"
        n2 = name + "_s2"
        return tf.Variable(initial_1, name=n1), tf.Variable(initial_2, name=n2)
    
    def bias_variable(self, shape, name="bias"):
        """Variable: shape = (input_dim, output_dim)"""
        initial = tf.constant(0.1, shape=shape)
        return tf.Variable(initial, name=name)
    
    def fc_weight_variable(self, in_dim, out_dim, name="fc_weight"):
        return tf.Variable(tf.random_normal([in_dim, out_dim]), name=name)
    
    def fc_bias_variable(self, out_dim, name="fc_bias"):
        return tf.Variable(tf.constant(0.1, shape=[out_dim]), name=name)
    
    def variable_summaries(self, var):
        """Summaries to evaluate filters with Tensorboard"""
        with tf.variable_scope('summaries'):
          mean = tf.reduce_mean(var)
          tf.summary.scalar('mean', mean)
          with tf.variable_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
          tf.summary.scalar('stddev', stddev)
          tf.summary.scalar('max', tf.reduce_max(var))
          tf.summary.scalar('min', tf.reduce_min(var))
          tf.summary.histogram('histogram', var)
          
    def conv2d(self, s1, s2, input_dim, output_dim, strides=1, scope='convolution'):
        """2-D convolution for hexagonal image processing
            with array set addressing""" 
        with tf.variable_scope(scope):
            with tf.variable_scope('weights'):
                w1, w2 = self.weight_variable([input_dim, output_dim], scope+"/filter")
                self.variable_summaries(w1)
                self.variable_summaries(w2)
#             with tf.variable_scope('biases'):
#                 b1 = self.bias_variable([output_dim], scope+"/bias_s1")
#                 self.variable_summaries(b1)
#                 b2 = self.bias_variable([output_dim], scope+"/bias_s2")
#                 self.variable_summaries(b2)
            with tf.variable_scope('subset_convolution'):
                with tf.variable_scope('convolve_set_1'):
                    s1_out_tmp_1 = tf.nn.conv2d(s1, w2, strides=[1, strides, strides, 1], padding='SAME')
                    s2_left_padded = tf.pad(s2, [[0,0], [1,0], [1,0], [0,0]])
                    s1_out_tmp_2 = tf.nn.conv2d(s2_left_padded, w1, strides=[1, strides, strides, 1], padding='VALID')
                    s1_out = tf.add(s1_out_tmp_1, s1_out_tmp_2)
                with tf.variable_scope('convolve_set_2'):
                    s2_out_tmp_1 = tf.nn.conv2d(s1, w1, strides=[1, strides, strides, 1], padding='SAME')
                    s2_out_tmp_2 = tf.nn.conv2d(s2, w2, strides=[1, strides, strides, 1], padding='SAME')
                    s2_out = tf.add(s2_out_tmp_1, s2_out_tmp_2)
#             return tf.nn.bias_add(s1_out, b1), tf.nn.bias_add(s2_out, b2)
                return s1_out, s2_out
            
    def relu(self, s1, s2):
        return tf.nn.relu(s1, name='relu_s1'), tf.nn.relu(s2, name='relu_s2')
    
    def conv_block(self, s1, s2, input_dim, output_dim, n_convs, phase_train, strides=1, scope='conv_block'):
        with tf.variable_scope(scope):
            name = scope + "/conv1"
            s1_conv, s2_conv = self.conv2d(s1, s2, input_dim, output_dim, scope=name)
            s1_norm, s2_norm = self.batch_norm(s1_conv, s2_conv, output_dim, phase_train, scope=name+'/batch_norm')
            s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
            for i in range(1,n_convs):
                name = scope + "/conv" + str(i+1)
                s1_conv, s2_conv = self.conv2d(s1_relu, s2_relu, output_dim, output_dim, scope=name)
                s1_norm, s2_norm = self.batch_norm(s1_conv, s2_conv, output_dim, phase_train, scope=name+'/batch_norm')
#                 s2_norm = self.batch_norm(s2_conv, output_dim, phase_train, scope=name+'/batch_norm_s2')
                s1_relu = tf.nn.relu(s1_norm, name=name+'/relu_s1')
                s2_relu = tf.nn.relu(s2_norm, name=name+'/relu_s2')
            return s1_relu, s2_relu
            
    
    def maxpool2d(self, s1, s2, k1=[2, 2], k2=[1, 3], scope='pooling'):
        """Maxpooling for hexagonal image processing
            with array set addressing""" 
        with tf.variable_scope(scope):
            with tf.variable_scope('pool_set_1'):
                tmp1 = tf.nn.max_pool(s1, ksize=[1, k2[0], k2[1], 1], strides=[1, 1, 1, 1],
                                      padding='SAME')
            with tf.variable_scope('pool_set_2'):
                s2_left_padded = tf.pad(s2, [[0,0], [1,0], [1,0], [0,0]])
                tmp2 = tf.nn.max_pool(s2_left_padded, ksize=[1, k1[0], k1[1], 1], strides=[1, 1, 1, 1],
                                      padding='VALID')
            with tf.variable_scope('maximum_addition'):
                tmp = tf.maximum(tmp1, tmp2)
            with tf.variable_scope('strided_slice'):
                with tf.variable_scope('slice_set_1'):
                    s1_out = tmp[::1, ::2, ::2, ::1]
                with tf.variable_scope('slice_set_2'):
                    s2_out = tmp[::1, 1::2, 1::2, ::1]
                    shape1 = s1_out.get_shape().as_list()
                    shape2 = s2_out.get_shape().as_list()
                    s2_out = tf.pad(s2_out, [[0, 0], [0, shape1[1]-shape2[1]], [0, shape1[2]-shape2[2]], [0, 0]])
            return s1_out, s2_out
    
    def batch_norm(self, s1, s2, n_out, phase_train, scope='batch_norm', affine=True):
        """Normalising batch with global normalisation"""
        x = tf.concat([s1, s2], 0)
        with tf.variable_scope(scope):
            beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
              name='beta', trainable=True)
            gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
              name='gamma', trainable=True)
            batch_mean, batch_var = tf.nn.moments(x, [0, 1, 2], name='moments')
            ema = tf.train.ExponentialMovingAverage(decay=0.99)
            def mean_var_with_update():
              ema_apply_op = ema.apply([batch_mean, batch_var])
              with tf.control_dependencies([ema_apply_op]):
                return tf.identity(batch_mean), tf.identity(batch_var)
            mean, var = tf.cond(phase_train,
              mean_var_with_update,
              lambda: (ema.average(batch_mean), ema.average(batch_var)))
            normed = tf.nn.batch_norm_with_global_normalization(x, mean, var,
              beta, gamma, 1e-3, affine)
        return tf.split(normed, 2)
    
    def telescope_model(self, s1, s2, dropout, phase_train):
        with tf.variable_scope("tel_model"):
            # Batch norm
            s1_norm, s2_norm = self.batch_norm(s1, s2, 1, phase_train, scope='batch_norm_s1_0')
            # Conv
            s1_conv, s2_conv = self.conv2d(s1_norm, s2_norm, 1, 128)
            s1_norm, s2_norm = self.batch_norm(s1_conv, s2_conv, 128, phase_train)
            s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
            # Max pool
            s1_max, s2_max = self.maxpool2d(s1_relu, s2_relu)
            # Conv block (3)
            s1_conv, s2_conv = self.conv2d(s1_max, s2_max, 128, 256)
            s1_norm, s2_norm = self.batch_norm(s1_conv, s2_conv, 256, phase_train)
            s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
            s1_conv, s2_conv = self.conv2d(s1_relu, s2_relu, 256, 256)
            s1_norm, s2_norm = self.batch_norm(s1_conv, s2_conv, 256, phase_train)
            s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
            s1_conv, s2_conv = self.conv2d(s1_relu, s2_relu, 256, 256)
            s1_norm, s2_norm = self.batch_norm(s1_conv, s2_conv, 256, phase_train)
            s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
            # Max pool
            s1_max, s2_max = self.maxpool2d(s1_relu, s2_relu)
            # Conv
            s1_conv, s2_conv = self.conv2d(s1_max, s2_max, 256, 512)
            s1_norm, s2_norm = self.batch_norm(s1_conv, s2_conv, 512, phase_train)
            s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
            # Max pool
            s1_max, s2_max = self.maxpool2d(s1_relu, s2_relu)
            # Batch norm
              
            with tf.variable_scope('fc_layer_1'):
                w_fcn = self.fc_weight_variable(2 * 3 * 4 * 512, 1024, "fc1_weight")
                self.variable_summaries(w_fcn)
                b_fcn = self.fc_bias_variable(1024, "fc1_bias")
                self.variable_summaries(b_fcn)
                s1_flat = tf.reshape(s1_max, [-1, w_fcn.get_shape().as_list()[0] / 2])
                s2_flat = tf.reshape(s2_max, [-1, w_fcn.get_shape().as_list()[0] / 2])
                s_concat = tf.concat([s1_flat, s2_flat], 1)
                fc1 = tf.add(tf.matmul(s_concat, w_fcn), b_fcn)
                fc1_relu = tf.nn.relu(fc1)
                fc1_drop = tf.nn.dropout(fc1_relu, dropout)
            with tf.variable_scope('fc_layer_2'):
                w_out = self.fc_weight_variable(1024, self.telescope_model_output_size, "fc2_weight")
                self.variable_summaries(w_out)
                b_out = self.fc_bias_variable(self.telescope_model_output_size, "fc2_bias")
                self.variable_summaries(b_out)
                out = tf.add(tf.matmul(fc1_drop, w_out), b_out)
        return out
    
    def image_batch_to_event_batch(self, telwd_batch, tel_response_batch):
        with tf.variable_scope('tel_to_array'):
#             tel_response_list = tf.unstack(tel_response_batch)
#             event_list = []
#             pos = 0
#             for event in range(self.batch_size):
#                 combined_tel_responses = []
#                 event_telwd = telwd_batch[event:event+1, ::]
#                 for tel in range(self.hess1_tels):
# #                     mask = tf.one_hot([tel], self.hess1_tels, on_value=1, off_value=0, axis=-1, dtype=tf.int32)
# #                     condition = tf.equal(tf.reduce_sum(tf.multiply(event_telwd, mask)), tf.cast(1, tf.int32))
#                     condition = tf.equal(event_telwd[0, tel], tf.cast(1, tf.int32))
#                     def tel_is_wd(tel_response_batch=tel_response_batch, pos=pos):
# #                         return tel_response_list.pop(0)
#                         return tel_response_batch[pos:pos+1, ::], pos+1
#                     def tel_not_wd(pos=pos):
#                         return tf.zeros(self.telescope_model_output_size), pos
#                     tel_response, pos = tf.cond(condition, tel_is_wd, tel_not_wd)
#                     combined_tel_responses.append(tel_response)
#                 event_list.append(tf.concat(combined_tel_responses, 0))
#             return tf.concat(event_list, 0)
            
            pos = [0]
#             def join_tel_response(event_telwd, tel_response_batch=tel_response_batch, pos=pos):
#                 combined_tel_responses = []
#                 for tel in range(self.hess1_tels):
# #                     mask = tf.one_hot([tel], self.hess1_tels, on_value=1, off_value=0, axis=-1, dtype=tf.int32)
# #                     condition = tf.equal(tf.reduce_sum(tf.multiply(event_telwd, mask)), tf.cast(1, tf.int32))
#                     condition = tf.equal(event_telwd[tel], tf.cast(1, tf.int32))
#                     def tel_is_wd(tel_response_batch=tel_response_batch, pos=pos):
# #                         return tel_response_list.pop(0)
#                         response = tel_response_batch[pos[0]:pos[0]+1, ::]
#                         pos[0] += 1
#                         return response
#                     def tel_not_wd():
#                         return tf.zeros(self.telescope_model_output_size)
#                     tel_response = tf.cond(condition, tel_is_wd, tel_not_wd)
#                     combined_tel_responses.append(tel_response)
#                 return tf.concat(combined_tel_responses, 0)
            def join2(event_telwd, tel_response_batch=tel_response_batch, pos=pos):
                def get_tel(tel_response_batch=tel_response_batch, pos=pos):
                    response = tel_response_batch[pos[0]:pos[0]+1, ::]
                    pos[0] += 1
                    return response
                def get_default():
                    return tf.zeros(self.telescope_model_output_size)
                tel_batch = tf.scan(lambda _, tel: tf.cond(tf.equal(tel, 1), get_tel, get_default), event_telwd)
                return tf.reshape(tel_batch, [self.hess1_tels * self.telescope_model_output_size])
#             initializer = tf.zeros([self.hess1_tels * self.telescope_model_output_size])
            event_batch = tf.scan(lambda _, event_telswd: join2(event_telswd), telwd_batch)
        #     return tf.concat(event_batches)
        return event_batch
    
    def array_model(self, event, dropout, scope='array_model'):
        with tf.variable_scope('array_model'):
            with tf.variable_scope('fc_layer_1'):
                w_fcn1 = tf.Variable(tf.random_normal([self.hess1_tels * self.telescope_model_output_size, 100]))
                b_fcn1 = tf.Variable(tf.random_normal([100]))
                print("event shape:", event.get_shape().as_list())
                fc1 = tf.add(tf.matmul(event, w_fcn1), b_fcn1)
                fc1 = tf.nn.relu(fc1)
                print("1. FCN")
                print("event shape:", fc1.get_shape().as_list())
            with tf.variable_scope('fc_layer_2'):
                w_fcn2 = tf.Variable(tf.random_normal([100, 10]))
                b_fcn2 = tf.Variable(tf.random_normal([10]))
        #         fc1 = tf.nn.dropout(fc1, dropout)
                fc2 = tf.add(tf.matmul(fc1, w_fcn2), b_fcn2)
                fc2 = tf.nn.relu(fc2)
                print("2. FCN")
                print("s1 shape:", fc2.get_shape().as_list())
            with tf.variable_scope('fc_layer_3'):
                w_out = tf.Variable(tf.random_normal([10, self.n_classes]))
                b_out = tf.Variable(tf.random_normal([self.n_classes]))
                fc2 = tf.nn.dropout(fc2, dropout)
                out = tf.add(tf.matmul(fc2, w_out), b_out)
                print("2. FCN")
                print("s1 shape:", out.get_shape().as_list())
            return out
    
    def process(self, telwd, s1, s2, dropout, phase_train):
#         s1 = tf.cast(s1, tf.float32)
#         s2 = tf.cast(s2, tf.float32)
#         telwd = tf.cast(telwd, tf.int32)
        tel_responses = self.telescope_model(s1, s2, dropout, phase_train)
        event_batch = self.image_batch_to_event_batch(telwd, tel_responses)
        return self.array_model(event_batch, dropout)