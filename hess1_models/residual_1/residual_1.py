import tensorflow as tf

class HESS:
    def __init__(self, tels=4, tel_out_dim=16, n_classes=2, n_res_blocks=1):
        self.hess1_tels = tels
        self.telescope_model_output_size = tel_out_dim
        self.n_classes = n_classes
        self.n_block=n_res_blocks

    def weight_variable(self, shape):
        """Next neighbour filter for hexagonal image processing
            with array set addressing.
            Variable": shape = (input_dim, output_dim)""" 
        shape_1 = [2, 2] + shape
        shape_2 = [1, 3] + shape
        initial_1 = tf.truncated_normal(shape_1, stddev=0.1)
        initial_2 = tf.truncated_normal(shape_2, stddev=0.1)
        return tf.Variable(initial_1), tf.Variable(initial_2)
    
    def bias_variable(self, shape):
        """Variable: shape = (input_dim, output_dim)"""
        initial = tf.constant(0.1, shape=shape)
        return tf.Variable(initial)
    
    def variable_summaries(self, var):
        """Summaries to evaluate filters with Tensorboard"""
        with tf.variable_scope('summaries'):
          mean = tf.reduce_mean(var)
          tf.summary.scalar('mean', mean)
          with tf.variable_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
          tf.summary.scalar('stddev', stddev)
          tf.summary.scalar('max', tf.reduce_max(var))
          tf.summary.scalar('min', tf.reduce_min(var))
          tf.summary.histogram('histogram', var)
          
    def conv2d(self, s1, s2, input_dim, output_dim, strides=1, scope='convolution'):
        """2-D convolution for hexagonal image processing
            with array set addressing""" 
        with tf.variable_scope(scope):
            with tf.variable_scope('weights'):
                w1, w2 = self.weight_variable([input_dim, output_dim])
                self.variable_summaries(w1)
                self.variable_summaries(w2)
            with tf.variable_scope('biases'):
                b1 = self.bias_variable([output_dim])
                self.variable_summaries(b1)
                b2 = self.bias_variable([output_dim])
                self.variable_summaries(b2)
            with tf.variable_scope('subset_convolution'):
                with tf.variable_scope('convolve_set_1'):
                    s1_out_tmp_1 = tf.nn.conv2d(s1, w2, strides=[1, strides, strides, 1], padding='SAME')
                    s1_out_tmp_2 = tf.nn.conv2d(s2, w1, strides=[1, strides, strides, 1], padding='SAME')
                    s1_out = tf.add(s1_out_tmp_1, s1_out_tmp_2)
                with tf.variable_scope('convolve_set_2'):
                    s2_out_tmp_1 = tf.nn.conv2d(s1, w1, strides=[1, strides, strides, 1], padding='SAME')
                    s2_out_tmp_2 = tf.nn.conv2d(s2, w2, strides=[1, strides, strides, 1], padding='SAME')
                    s2_out = tf.add(s2_out_tmp_1, s2_out_tmp_2)
            return tf.nn.bias_add(s1_out, b1), tf.nn.bias_add(s2_out, b2)
    
    
    def maxpool2d(self, s1, s2, k1=[2, 2], k2=[1, 3], scope='pooling'):
        """Maxpooling for hexagonal image processing
            with array set addressing""" 
        with tf.variable_scope(scope):
            with tf.variable_scope('pool_set_1'):
                tmp1 = tf.nn.max_pool(s1, ksize=[1, k2[0], k2[1], 1], strides=[1, 1, 1, 1],
                                      padding='SAME')
            with tf.variable_scope('pool_set_2'):
                tmp2 = tf.nn.max_pool(s2, ksize=[1, k1[0], k1[1], 1], strides=[1, 1, 1, 1],
                                      padding='SAME')
            with tf.variable_scope('maximum_addition'):
                tmp = tf.maximum(tmp1, tmp2)
            with tf.variable_scope('strided_slice'):
                with tf.variable_scope('slice_set_1'):
                    end1 = tmp.get_shape().as_list()
                    end1[1] -= 2 * (end1[1] % 2)
                    end1[2] -= 2 * (end1[2] % 2)
                    s1_out = tf.strided_slice(tmp, [0, 0, 0, 0], end1, [1, 2, 2, 1])
                with tf.variable_scope('slice_set_2'):
                    end2 = tmp.get_shape().as_list()
                    s2_out = tf.strided_slice(tmp, [0, 1, 1, 0], end2, [1, 2, 2, 1])
            return s1_out, s2_out
    
    def batch_norm(self, x, n_out, phase_train, scope='batch_norm', affine=True):
        """Normalising batch with global normalisation"""
        with tf.variable_scope(scope):
            beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
              name='beta', trainable=True)
            gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
              name='gamma', trainable=True)
            batch_mean, batch_var = tf.nn.moments(x, [0, 1, 2], name='moments')
            ema = tf.train.ExponentialMovingAverage(decay=0.99)
            def mean_var_with_update():
              ema_apply_op = ema.apply([batch_mean, batch_var])
              with tf.control_dependencies([ema_apply_op]):
                return tf.identity(batch_mean), tf.identity(batch_var)
            mean, var = tf.cond(phase_train,
              mean_var_with_update,
              lambda: (ema.average(batch_mean), ema.average(batch_var)))
            normed = tf.nn.batch_norm_with_global_normalization(x, mean, var,
              beta, gamma, 1e-3, affine)
        return normed
      
    def residual_block(self, s1, s2, n_in, n_out, subsample, phase_train, scope='res_block'):
        with tf.variable_scope(scope):
            if subsample:
              y1, y2 = self.conv2d(s1, s2, n_in, n_out)
              shortcut1, shortcut2 = self.conv2d(s1, s2, n_in, n_out)
            else:
              y1, y2 = self.conv2d(s1, s2, n_in, n_out)
              shortcut1 = tf.identity(s1, name='shortcut1')
              shortcut2 = tf.identity(s2, name='shortcut2')
            y1 = self.batch_norm(y1, n_out, phase_train, scope='batch_norm_1')
            y2 = self.batch_norm(y2, n_out, phase_train, scope='batch_norm_1')
            y1 = tf.nn.relu(y1, name='relu_1')
            y2 = tf.nn.relu(y2, name='relu_1')
            tf.summary.histogram('activations1_1', y1)
            tf.summary.histogram('activations1_2', y2)
            y1, y2 = self.conv2d(y1, y2, n_out, n_out)
            y1 = self.batch_norm(y1, n_out, phase_train, scope='batch_norm_2')
            y2 = self.batch_norm(y2, n_out, phase_train, scope='batch_norm_2')
            y1 = tf.add(y1, shortcut1)
            y2 = tf.add(y2, shortcut2)
            y1 = tf.nn.relu(y1, name='relu_2')
            y2 = tf.nn.relu(y2, name='relu_2')
            tf.summary.histogram('activations2_1', y1)
            tf.summary.histogram('activations2_2', y2)
        return y1, y2 
    
    def residual_group(self, s1, s2, n_in, n_out, first_subsample, phase_train, scope='res_group'):
        with tf.variable_scope(scope):
            y1, y2 = self.residual_block(s1, s2, n_in, n_out, first_subsample, phase_train, scope='res_block_1')
            for i in xrange(self.n_block - 1):
              y1, y2 = self.residual_block(y1, y2, n_out, n_out, False, phase_train, scope='res_block_%d' % (i + 2))
        return y1, y2
    
    def telescope_model(self, s1, s2, dropout, phase_train):
        with tf.variable_scope("tel_model"):
            print("s1 shape:", s1.get_shape().as_list())
            print("s2 shape:", s2.get_shape().as_list())
            with tf.variable_scope('convolution_1'):  
                y1, y2 = self.conv2d(s1, s2, 1, 64)
                y1 = self.batch_norm(y1, 64, phase_train, scope='batch_norm_init_s1')
                y2 = self.batch_norm(y2, 64, phase_train, scope='batch_norm_init_s2')
                y1 = tf.nn.relu(y1, name='relu_init_s1')
                y2 = tf.nn.relu(y2, name='relu_init_s2')
                print("1. convolution")
                print("s1 shape:", y1.get_shape().as_list())
                print("s2 shape:", y2.get_shape().as_list())
            y1, y2 = self.residual_group(y1, y2, 64, 64, False, phase_train, scope='res_group_1')
            print("1. residual")
            print("s1 shape:", y1.get_shape().as_list())
            print("s2 shape:", y2.get_shape().as_list())
            y1, y2 = self.maxpool2d(y1, y2, scope='pool_1')
            print("1. pooling")
            print("s1 shape:", y1.get_shape().as_list())
            print("s2 shape:", y2.get_shape().as_list())
            y1, y2 = self.residual_group(y1, y2, 64, 128, True, phase_train, scope='res_group_2')
            print("2. residual")
            print("s1 shape:", y1.get_shape().as_list())
            print("s2 shape:", y2.get_shape().as_list())
            y1, y2 = self.maxpool2d(y1, y2, scope='pool_2')
            y1, y2 = self.residual_group(y1, y2, 128, 256, True, phase_train, scope='res_group_3')
            print("2. pooling")
            print("s1 shape:", y1.get_shape().as_list())
            print("s2 shape:", y2.get_shape().as_list())
            with tf.variable_scope('fc_layer_1'):
                w_fcn = tf.Variable(tf.random_normal([2 * 8 * 4 * 256, 1024]))
                b_fcn = tf.Variable(tf.random_normal([1024]))
                fc1_1 = tf.reshape(y1, [-1, w_fcn.get_shape().as_list()[0] / 2])
                fc1_2 = tf.reshape(y2, [-1, w_fcn.get_shape().as_list()[0] / 2])
                print("Flatten")
                print("s1 shape:", fc1_1.get_shape().as_list())
                print("s2 shape:", fc1_2.get_shape().as_list())
                fc1 = tf.concat([fc1_1, fc1_2], 1)
                print("Concat")
                print("s1 shape:", fc1.get_shape().as_list())
                fc1 = tf.add(tf.matmul(fc1, w_fcn), b_fcn)
                fc1 = tf.nn.relu(fc1)
                print("1. FCN")
                print("s1 shape:", fc1.get_shape().as_list())
            with tf.variable_scope('fc_layer_2'):
                w_out = tf.Variable(tf.random_normal([1024, self.telescope_model_output_size]))
                b_out = tf.Variable(tf.random_normal([self.telescope_model_output_size]))
                fc1 = tf.nn.dropout(fc1, dropout)
                out = tf.add(tf.matmul(fc1, w_out), b_out)
                print("2. FCN")
                print("s1 shape:", out.get_shape().as_list())
        return out
    
    def image_batch_to_event_batch(self, telwd_batch, tel_response_batch):
        with tf.variable_scope('tel_to_array'):
            tel_response_list = tf.unstack(tel_response_batch)
            def join_tel_response(event_telwd, tel_response_list=tel_response_list):
                combined_tel_responses = []
                for tel in range(self.hess1_tels):
                    mask = tf.one_hot([tel], self.hess1_tels, on_value=1, off_value=0, axis=-1, dtype=tf.int32)
                    condition = tf.equal(tf.reduce_sum(tf.multiply(event_telwd, mask)), tf.cast(1, tf.int32))
                    def tel_is_wd(tel_response_list=tel_response_list):
                        return tel_response_list.pop(0)
                    def tel_not_wd():
                        return tf.zeros(self.telescope_model_output_size)
                    tel_response = tf.cond(condition, tel_is_wd, tel_not_wd)
                    combined_tel_responses.append(tel_response)
                return tf.concat(combined_tel_responses, 0)
            initializer = tf.constant(0, dtype=tf.float32, shape=[self.hess1_tels * self.telescope_model_output_size])
            event_batch = tf.scan(lambda _, event_telswd: join_tel_response(event_telswd), telwd_batch, initializer)
        #     return tf.concat(event_batches)
        return event_batch
    
    def array_model(self, event, dropout, scope='array_model'):
        with tf.variable_scope('array_model'):
            with tf.variable_scope('fc_layer_1'):
                w_fcn1 = tf.Variable(tf.random_normal([self.hess1_tels * self.telescope_model_output_size, 2048]))
                b_fcn1 = tf.Variable(tf.random_normal([2048]))
                print("event shape:", event.get_shape().as_list())
                fc1 = tf.add(tf.matmul(event, w_fcn1), b_fcn1)
                fc1 = tf.nn.relu(fc1)
                print("1. FCN")
                print("event shape:", fc1.get_shape().as_list())
            with tf.variable_scope('fc_layer_2'):
                w_fcn2 = tf.Variable(tf.random_normal([2048, 1024]))
                b_fcn2 = tf.Variable(tf.random_normal([1024]))
        #         fc1 = tf.nn.dropout(fc1, dropout)
                fc2 = tf.add(tf.matmul(fc1, w_fcn2), b_fcn2)
                fc2 = tf.nn.relu(fc2)
                print("2. FCN")
                print("s1 shape:", fc2.get_shape().as_list())
            with tf.variable_scope('fc_layer_3'):
                w_out = tf.Variable(tf.random_normal([1024, self.n_classes]))
                b_out = tf.Variable(tf.random_normal([self.n_classes]))
                fc2 = tf.nn.dropout(fc2, dropout)
                out = tf.add(tf.matmul(fc2, w_out), b_out)
                print("2. FCN")
                print("s1 shape:", out.get_shape().as_list())
            return out
    
    def process(self, telwd, s1, s2, dropout, phase_train):
#         s1 = tf.cast(s1, tf.float32)
#         s2 = tf.cast(s2, tf.float32)
#         telwd = tf.cast(telwd, tf.int32)
        tel_responses = self.telescope_model(s1, s2, dropout, phase_train)
        event_batch = self.image_batch_to_event_batch(telwd, tel_responses)
        return self.array_model(event_batch, dropout)