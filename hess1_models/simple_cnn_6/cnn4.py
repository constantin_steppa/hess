import tensorflow as tf

class HESS:
    def __init__(self, batch_size, telescope_model_output_size, n_classes=2):
        self.hess1_tels = 4
        self.telescope_model_output_size = telescope_model_output_size
        self.n_classes = n_classes
        self.batch_size = batch_size

    def weight_variable_1n(self, shape):
        """Next neighbour filter for hexagonal image processing
            with array set addressing.
            Variable": shape = (input_dim, output_dim)""" 
        shape_1 = [2, 2] + shape
        shape_2 = [1, 3] + shape
        w1 = tf.truncated_normal(shape_1, stddev=0.1)
        w2 = tf.truncated_normal(shape_2, stddev=0.1)
        return tf.Variable(w1, name="w1"), tf.Variable(w2, name="w2")
    
    def weight_variable_2n(self, shape):
        """Second next neighbour filter for hexagonal image processing
            with array set addressing.
            Variable": shape = (input_dim, output_dim)""" 
        shape_1 = [1, 3] + shape
        shape_2 = [2, 4] + shape
        shape_3 = [1, 5] + shape
        w1_1 = tf.truncated_normal(shape_1, stddev=0.1)
        w1_2 = tf.truncated_normal(shape_3, stddev=0.1)
        w1_3 = tf.truncated_normal(shape_1, stddev=0.1)
        w2 = tf.truncated_normal(shape_2, stddev=0.1)
        return tf.Variable(w1_1, name="w1_1"), tf.Variable(w1_2, name="w1_2"), tf.Variable(w1_3, name="w1_3"), tf.Variable(w2, name="w2")
    
    def bias_variable(self, shape, name="bias"):
        """Variable: shape = (input_dim, output_dim)"""
        initial = tf.constant(0.1, shape=shape)
        return tf.Variable(initial, name=name)
    
    def fc_weight_variable(self, in_dim, out_dim, name="fc_weight"):
        return tf.Variable(tf.random_normal([in_dim, out_dim]), name=name)
    
    def fc_bias_variable(self, out_dim, name="fc_bias"):
        return tf.Variable(tf.constant(0.1, shape=[out_dim]), name=name)
    
    def variable_summaries(self, var):
        """Summaries to evaluate filters with Tensorboard"""
        with tf.variable_scope('summaries'):
          mean = tf.reduce_mean(var)
          tf.summary.scalar('mean', mean)
          with tf.variable_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
          tf.summary.scalar('stddev', stddev)
          tf.summary.scalar('max', tf.reduce_max(var))
          tf.summary.scalar('min', tf.reduce_min(var))
          tf.summary.histogram('histogram', var)
          
    def conv2d_1n(self, s1, s2, input_dim, output_dim, strides=1, scope='conv2d_1n'):
        """2-D convolution for hexagonal image processing
            with array set addressing""" 
        with tf.variable_scope(scope):
            with tf.variable_scope('weights'):
                w1, w2 = self.weight_variable_1n([input_dim, output_dim])
                self.variable_summaries(w1)
                self.variable_summaries(w2)
            with tf.variable_scope('subset_convolution'):
                with tf.variable_scope('convolve_set_1'):
                    s1_out_tmp_1 = tf.nn.conv2d(s1, w2, strides=[1, strides, strides, 1], padding='SAME')
                    s2_left_padded = tf.pad(s2, [[0,0], [1,0], [1,0], [0,0]])
                    s1_out_tmp_2 = tf.nn.conv2d(s2_left_padded, w1, strides=[1, strides, strides, 1], padding='VALID')
                    s1_out = tf.add(s1_out_tmp_1, s1_out_tmp_2)
                with tf.variable_scope('convolve_set_2'):
                    s2_out_tmp_1 = tf.nn.conv2d(s1, w1, strides=[1, strides, strides, 1], padding='SAME')
                    s2_out_tmp_2 = tf.nn.conv2d(s2, w2, strides=[1, strides, strides, 1], padding='SAME')
                    s2_out = tf.add(s2_out_tmp_1, s2_out_tmp_2)
                return s1_out, s2_out
            
    def conv2d_2n(self, s1, s2, input_dim, output_dim, strides=1, scope='conv2d_2n'):
        """2-D convolution for hexagonal image processing
            with array set addressing""" 
        with tf.variable_scope(scope):
            with tf.variable_scope('weights'):
                w1_1, w1_2, w1_3, w2 = self.weight_variable_2n([input_dim, output_dim])
            with tf.variable_scope('subset_convolution'):
                with tf.variable_scope('convolve_set_1'):
                    tmp = s1[:, :-1, :, :]
                    tmp = tf.nn.conv2d(tmp, w1_1, strides=[1, strides, strides, 1], padding='SAME')
                    s1_1 = tf.pad(tmp, [[0,0], [1,0], [0,0], [0,0]])
                    tmp = s1[:, 1:, :, :]
                    tmp = tf.nn.conv2d(tmp, w1_3, strides=[1, strides, strides, 1], padding='SAME')
                    s1_2 = tf.pad(tmp, [[0,0], [0,1], [0,0], [0,0]])
                    s1_3 = tf.nn.conv2d(s1, w1_2, strides=[1, strides, strides, 1], padding='SAME')
                    tmp = tf.pad(s2, [[0,0], [1,0], [2,1], [0,0]])
                    s1_4 = tf.nn.conv2d(tmp, w2, strides=[1, strides, strides, 1], padding='VALID')
                    s1_out = tf.add_n([s1_1, s1_2, s1_3, s1_4])
                with tf.variable_scope('convolve_set_2'):
                    tmp = s2[:, :-1, :, :]
                    tmp = tf.nn.conv2d(tmp, w1_1, strides=[1, strides, strides, 1], padding='SAME')
                    s2_1 = tf.pad(tmp, [[0,0], [1,0], [0,0], [0,0]])
                    tmp = s2[:, 1:, :, :]
                    tmp = tf.nn.conv2d(tmp, w1_3, strides=[1, strides, strides, 1], padding='SAME')
                    s2_2 = tf.pad(tmp, [[0,0], [0,1], [0,0], [0,0]])
                    s2_3 = tf.nn.conv2d(s2, w1_2, strides=[1, strides, strides, 1], padding='SAME')
                    tmp = tf.pad(s1, [[0,0], [0,1], [1,2], [0,0]])
                    s2_4 = tf.nn.conv2d(tmp, w2, strides=[1, strides, strides, 1], padding='VALID')
                    s2_out = tf.add_n([s2_1, s2_2, s2_3, s2_4])
                return s1_out, s2_out
            
    def relu(self, s1, s2):
        return tf.nn.relu(s1, name='relu_s1'), tf.nn.relu(s2, name='relu_s2')
    
#     def conv_block(self, s1, s2, input_dim, output_dim, n_convs, phase_train, strides=1, scope='conv_block'):
#         with tf.variable_scope(scope):
#             name = scope + "/conv1"
#             s1_conv, s2_conv = self.conv2d_1n(s1, s2, input_dim, output_dim, scope=name)
#             s1_norm, s2_norm = self.batch_norm(s1_conv, s2_conv, output_dim, phase_train, scope=name+'/batch_norm')
#             s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
#             for i in range(1,n_convs):
#                 name = scope + "/conv" + str(i+1)
#                 s1_conv, s2_conv = self.conv2d_1n(s1_relu, s2_relu, output_dim, output_dim, scope=name)
#                 s1_norm, s2_norm = self.batch_norm(s1_conv, s2_conv, output_dim, phase_train, scope=name+'/batch_norm')
#                 s1_relu = tf.nn.relu(s1_norm, name=name+'/relu_s1')
#                 s2_relu = tf.nn.relu(s2_norm, name=name+'/relu_s2')
#             return s1_relu, s2_relu
            
    
    def maxpool2d(self, s1, s2, k1=[2, 2], k2=[1, 3], scope='pooling'):
        """Maxpooling for hexagonal image processing
            with array set addressing""" 
        with tf.variable_scope(scope):
            with tf.variable_scope('pool_set_1'):
                tmp1 = tf.nn.max_pool(s1, ksize=[1, k2[0], k2[1], 1], strides=[1, 1, 1, 1],
                                      padding='SAME')
            with tf.variable_scope('pool_set_2'):
                s2_left_padded = tf.pad(s2, [[0,0], [1,0], [1,0], [0,0]])
                tmp2 = tf.nn.max_pool(s2_left_padded, ksize=[1, k1[0], k1[1], 1], strides=[1, 1, 1, 1],
                                      padding='VALID')
            with tf.variable_scope('maximum_addition'):
                tmp = tf.maximum(tmp1, tmp2)
            with tf.variable_scope('strided_slice'):
                with tf.variable_scope('slice_set_1'):
                    s1_out = tmp[::1, ::2, ::2, ::1]
                with tf.variable_scope('slice_set_2'):
                    s2_out = tmp[::1, 1::2, 1::2, ::1]
                    shape1 = s1_out.get_shape().as_list()
                    shape2 = s2_out.get_shape().as_list()
                    s2_out = tf.pad(s2_out, [[0, 0], [0, shape1[1]-shape2[1]], [0, shape1[2]-shape2[2]], [0, 0]])
            return s1_out, s2_out
    
    def batch_norm_asa(self, s1, s2, n_out, phase_train, scope='batch_norm_asa', affine=True):
        """Normalising batch with global normalisation"""
        x = tf.concat([s1, s2], 0)
        with tf.variable_scope(scope):
            beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
              name='beta', trainable=True)
            gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
              name='gamma', trainable=True)
            batch_mean, batch_var = tf.nn.moments(x, [0, 1, 2], name='moments')
            ema = tf.train.ExponentialMovingAverage(decay=0.99)
            def mean_var_with_update():
              ema_apply_op = ema.apply([batch_mean, batch_var])
              with tf.control_dependencies([ema_apply_op]):
                return tf.identity(batch_mean), tf.identity(batch_var)
            mean, var = tf.cond(phase_train,
              mean_var_with_update,
              lambda: (ema.average(batch_mean), ema.average(batch_var)))
            normed = tf.nn.batch_norm_with_global_normalization(x, mean, var,
              beta, gamma, 1e-3, affine)
        return tf.split(normed, 2)
    
    def batch_norm(self, x, n_out, phase_train, scope='batch_norm_asa', affine=True):
        """Normalising batch with global normalisation"""
        with tf.variable_scope(scope):
            beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
              name='beta', trainable=True)
            gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
              name='gamma', trainable=True)
            batch_mean, batch_var = tf.nn.moments(x, [0, 1], name='moments')
            ema = tf.train.ExponentialMovingAverage(decay=0.99)
            def mean_var_with_update():
              ema_apply_op = ema.apply([batch_mean, batch_var])
              with tf.control_dependencies([ema_apply_op]):
                return tf.identity(batch_mean), tf.identity(batch_var)
            mean, var = tf.cond(phase_train,
              mean_var_with_update,
              lambda: (ema.average(batch_mean), ema.average(batch_var)))
            normed = tf.nn.batch_norm_with_global_normalization(x, mean, var,
              beta, gamma, 1e-3, affine)
        return normed
    
    def telescope_model(self, s1, s2, dropout, phase_train):
        with tf.variable_scope("tel_model"):
            # Batch norm
            s1_norm, s2_norm = self.batch_norm_asa(s1, s2, 1, phase_train)
            # Conv
            s1_conv, s2_conv = self.conv2d_2n(s1_norm, s2_norm, 1, 128)
            s1_norm, s2_norm = self.batch_norm_asa(s1_conv, s2_conv, 128, phase_train)
            s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
            # Max pool
            s1_max, s2_max = self.maxpool2d(s1_relu, s2_relu)
            # Conv block (3)
            s1_conv, s2_conv = self.conv2d_1n(s1_max, s2_max, 128, 256)
            s1_norm, s2_norm = self.batch_norm_asa(s1_conv, s2_conv, 256, phase_train)
            s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
            s1_conv, s2_conv = self.conv2d_1n(s1_relu, s2_relu, 256, 256)
            s1_norm, s2_norm = self.batch_norm_asa(s1_conv, s2_conv, 256, phase_train)
            s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
            s1_conv, s2_conv = self.conv2d_1n(s1_relu, s2_relu, 256, 256)
            s1_norm, s2_norm = self.batch_norm_asa(s1_conv, s2_conv, 256, phase_train)
            s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
            # Max pool
            s1_max, s2_max = self.maxpool2d(s1_relu, s2_relu)
            # Conv
            s1_conv, s2_conv = self.conv2d_1n(s1_max, s2_max, 256, 512)
            s1_norm, s2_norm = self.batch_norm_asa(s1_conv, s2_conv, 512, phase_train)
            s1_relu, s2_relu = self.relu(s1_norm, s2_norm)
            # Max pool
            s1_max, s2_max = self.maxpool2d(s1_relu, s2_relu)
            # Batch norm
              
            with tf.variable_scope('fc_layer_1'):
                w_fcn = self.fc_weight_variable(2 * 3 * 4 * 512, 1024, "fc1_weight")
                self.variable_summaries(w_fcn)
#                 b_fcn = self.fc_bias_variable(1024, "fc1_bias")
#                 self.variable_summaries(b_fcn)
                s1_flat = tf.reshape(s1_max, [-1, w_fcn.get_shape().as_list()[0] / 2])
                s2_flat = tf.reshape(s2_max, [-1, w_fcn.get_shape().as_list()[0] / 2])
                s_concat = tf.concat([s1_flat, s2_flat], 1)
#                 fc1 = tf.add(tf.matmul(s_concat, w_fcn), b_fcn)
                fc1 = tf.matmul(s_concat, w_fcn)
                fc1_norm = self.batch_norm(fc1, 1024, phase_train)
                fc1_relu = tf.nn.relu(fc1_norm)
            with tf.variable_scope('fc_layer_2'):
                w_fc2 = self.fc_weight_variable(1024, 100, "fc2_weight")
                self.variable_summaries(w_fc2)
                fc2 = tf.matmul(fc1_relu, w_fc2)
                fc2_norm = self.batch_norm(fc2, 100, phase_train)
                fc2_relu = tf.nn.relu(fc2_norm)
            with tf.variable_scope('fc_layer_3'):
                w_out = self.fc_weight_variable(100, self.telescope_model_output_size, "fc3_weight")
                self.variable_summaries(w_out)
                b_out = self.fc_bias_variable(self.telescope_model_output_size, "fc3_bias")
                self.variable_summaries(b_out)
                out = tf.add(tf.matmul(fc2_relu, w_out), b_out)
        return out

    
    def image_batch_to_event_batch(self, telwd_batch, tel_response_batch):
        with tf.variable_scope('tel_to_array'):
            values = tf.reshape([tel_response_batch], [-1])
            dense_shape=[self.batch_size, self.hess1_tels * self.telescope_model_output_size]
            print("values shape:", values.get_shape().as_list())
            print("dense shape:", dense_shape)
            print("indices shape:", telwd_batch.get_shape().as_list())
            tmp = tf.SparseTensor(indices=telwd_batch, values=values, dense_shape=dense_shape)
            return tf.sparse_tensor_to_dense(tmp, default_value=0)
            
#             indices = []
#             for event in range(self.batch_size):
#                 for tel in range(self.hess1_tels):
#                     condition = tf.equal(telwd_batch[event, tel], tf.cast(1, tf.int32))
#                     def tel_is_wd(indices=indices):
#                         list = [[event, self.telescope_model_output_size*tel+i] for i in range (self.telescope_model_output_size)]
#                         indices.append(list)
#                         return indices
#                     def tel_not_wd(indices=indices):
#                         return indices
#                     indices = tf.cond(condition, tel_is_wd, tel_not_wd)
#             values = tf.reshape([tel_response_batch], [-1])
#             tmp = tf.SparseTensor(indices=indices, values=values, dense_shape=[self.batch_size, self.hess1_tels * self.telescope_model_output_size])
#             return tf.sparse_tensor_to_dense(tmp, default_value=0)
#             tel_response_list = tf.unstack(tel_response_batch, tf.reduce_sum(telwd_batch))
#             event_list = []
#                 pos = 0
#            
#                 combined_tel_responses = []
#                 event_telwd = telwd_batch[event:event+1, ::]
#                
#                     mask = tf.one_hot([tel], self.hess1_tels, on_value=1, off_value=0, axis=-1, dtype=tf.int32)
#                     condition = tf.equal(tf.reduce_sum(tf.multiply(event_telwd, mask)), tf.cast(1, tf.int32))
#                     condition = tf.equal(event_telwd[0, tel], tf.cast(1, tf.int32))
#                    
#                    
#                         return tel_response_list.pop(0)
#                         def tel_is_wd(tel_response_batch=tel_response_batch, pos=pos):
#                             return tel_response_batch[pos:pos+1, ::], pos+1
#                    
#                         return tf.zeros(self.telescope_model_output_size)
#                         def tel_not_wd(pos=pos):
#                             return tf.zeros(self.telescope_model_output_size), pos
#                    
#                         tel_response, pos = tf.cond(condition, tel_is_wd, tel_not_wd)
#                     combined_tel_responses.append(tel_response)
#                    
#                 event_list.append(tf.concat(combined_tel_responses, 0))
#             return tf.stack(event_list)
#      
#             pos = [0]
# #             def join_tel_response(event_telwd, tel_response_batch=tel_response_batch, pos=pos):
# #                 combined_tel_responses = []
# #                 for tel in range(self.hess1_tels):
# # #                     mask = tf.one_hot([tel], self.hess1_tels, on_value=1, off_value=0, axis=-1, dtype=tf.int32)
# # #                     condition = tf.equal(tf.reduce_sum(tf.multiply(event_telwd, mask)), tf.cast(1, tf.int32))
# #                     condition = tf.equal(event_telwd[tel], tf.cast(1, tf.int32))
# #                     def tel_is_wd(tel_response_batch=tel_response_batch, pos=pos):
# # #                         return tel_response_list.pop(0)
# #                         response = tel_response_batch[pos[0]:pos[0]+1, ::]
# #                         pos[0] += 1
# #                         return response
# #                     def tel_not_wd():
# #                         return tf.zeros(self.telescope_model_output_size)
# #                     tel_response = tf.cond(condition, tel_is_wd, tel_not_wd)
# #                     combined_tel_responses.append(tel_response)
# #                 return tf.concat(combined_tel_responses, 0)
#             def join2(event_telwd, tel_response_batch=tel_response_batch, pos=pos):
#                 def get_tel(tel_response_batch=tel_response_batch, pos=pos):
#                     response = tel_response_batch[pos[0]:pos[0]+1, ::]
#                     pos[0] += 1
#                     return response
#                 def get_default():
#                     return tf.zeros(self.telescope_model_output_size)
#                 tel_batch = tf.scan(lambda _, tel: tf.cond(tf.equal(tel, 1), get_tel, get_default), event_telwd)
#                 return tf.reshape(tel_batch, [self.hess1_tels * self.telescope_model_output_size])
# #             initializer = tf.zeros([self.hess1_tels * self.telescope_model_output_size])
#             event_batch = tf.scan(lambda _, event_telswd: join2(event_telswd), telwd_batch)
#         #     return tf.concat(event_batches)
#         return event_batch
    
    def array_model(self, event, dropout, phase_train, scope='array_model'):
        with tf.variable_scope(scope):
            with tf.variable_scope('fc_layer_1'):
                event_norm = self.batch_norm(event, self.hess1_tels * self.telescope_model_output_size, phase_train)
                w_fc1 = tf.Variable(tf.random_normal([self.hess1_tels * self.telescope_model_output_size, 64]))
                self.variable_summaries(w_fc1)
                print("event shape:", event_norm.get_shape().as_list())
                event_con = tf.matmul(event_norm, w_fc1)
                event_relu = tf.nn.relu(event_con)
                print("1. FCN")
                print("event shape:", event_relu.get_shape().as_list())
            with tf.variable_scope('fc_layer_2'):
                event_norm = self.batch_norm(event_relu, 64, phase_train)
                w_fc2 = tf.Variable(tf.random_normal([64, 32]))
                self.variable_summaries(w_fc2)
                print("event shape:", event_norm.get_shape().as_list())
                event_con = tf.matmul(event_norm, w_fc2)
                event_relu = tf.nn.relu(event_con)
                print("2. FCN")
                print("event shape:", event_relu.get_shape().as_list())
            with tf.variable_scope('fc_layer_3'):
                event_norm = self.batch_norm(event_relu, 32, phase_train)
                w_fc3 = tf.Variable(tf.random_normal([32, 16]))
                self.variable_summaries(w_fc3)
                print("event shape:", event_norm.get_shape().as_list())
                event_con = tf.matmul(event_norm, w_fc3)
                event_relu = tf.nn.relu(event_con)
                print("3. FCN")
                print("event shape:", event_relu.get_shape().as_list())
            with tf.variable_scope('fc_layer_3'):
                w_out = tf.Variable(tf.random_normal([16, self.n_classes]))
                self.variable_summaries(w_out)
                b_out = tf.Variable(tf.random_normal([self.n_classes]))
                self.variable_summaries(b_out)
                out = tf.add(tf.matmul(event_relu, w_out), b_out)
                print("4. FCN")
                print("s1 shape:", out.get_shape().as_list())
            return out
    
    def process(self, telwd, s1, s2, dropout, phase_train):
#         s1 = tf.cast(s1, tf.float32)
#         s2 = tf.cast(s2, tf.float32)
#         telwd = tf.cast(telwd, tf.int32)
        tel_responses = self.telescope_model(s1, s2, dropout, phase_train)
        event_batch = self.image_batch_to_event_batch(telwd, tel_responses)
        return self.array_model(event_batch, dropout, phase_train)