from __future__ import print_function

import tensorflow as tf
import root_reader
import cnn4 as cnn
import math, os

# machine directories
machine = "local"
# machine = "zeuthen"
directory = "/Users/steppa/tf_examples/sample_2e4"
model_path = "/Users/steppa/git/hess/hess1_models/simple_cnn_6"
logdir = model_path +"/log/conv_1_3_1_wd_bn_lrd"
model_weights = model_path + "/weights/conv_1_3_1_wd_bn_lrd"
if(machine == "zeuthen"):
    directory = "/lustre/fs20/group/hess/user/steppa/tf_examples/root/sample_1e5"
    model_path = "/afs/ifh.de/group/hess/scratch/user/steppa/scripts/hess/hess1_models/simple_cnn_6"
    logdir = model_path + "/log/conv_1_3_1_wd_bn_lrd"
    model_weights = model_path + "/weights/conv_1_3_1_wd_bn_lrd"
    
train_file_list = []
for filename in os.listdir(directory+"/train"):
    train_file_list += [directory + "/train/" + filename]
train_reader = root_reader.Reader(train_file_list, name="train_reader")
valid_file_list = []
for filename in os.listdir(directory+"/valid"):
    valid_file_list += [directory + "/valid/" + filename]
valid_reader = root_reader.Reader(valid_file_list, name="valid_reader")

# Parameters
learning_rate = 1e-4
epochs = 5
training_iters = 5000
# training_iters = train_reader.get_entries()
batch_size = 100
display_step = 2
max_steps = training_iters / batch_size

# global_step = tf.Variable(0, trainable=False)
# starter_learning_rate = 1.5e-3
# learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step,
#                                            50, 0.8, staircase=True)

# Network Parameters
n_classes = 2  # gammas and background
telescope_model_output_size = 10
dropout = 0.75  # Dropout, probability to keep units

# tf Graph input
with tf.variable_scope('input'):
    s1_ = tf.placeholder(tf.float32, [None, 18, 32, 1])
    s2_ = tf.placeholder(tf.float32, [None, 18, 32, 1])
    telwd_ = tf.placeholder(tf.int64, [None, 2])
    y_ = tf.placeholder(tf.int64, [None])
    keep_prob_ = tf.placeholder(tf.float32)
    training_ = tf.placeholder(tf.bool)

model = cnn.HESS(batch_size=batch_size, telescope_model_output_size=telescope_model_output_size, n_classes=n_classes)
pred = model.process(telwd=telwd_, s1=s1_, s2=s2_, dropout=keep_prob_, phase_train=training_)
with tf.variable_scope('cost'):
    with tf.variable_scope('weight_decay'):
        vars   = tf.trainable_variables() 
        lossL2 = tf.add_n([ tf.nn.l2_loss(v) for v in vars
                            if 'beta' not in v.name ]) * 0.001
    with tf.variable_scope('cross_entropy'):
        cost = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=pred, labels=y_)) + lossL2
        tf.summary.scalar('cross_entropy', cost)
with tf.variable_scope('train'):
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
#     optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost,  global_step=global_step)
with tf.variable_scope('accuracy'):
    with tf.variable_scope('correct_prediction'):
      correct_pred = tf.equal(tf.argmax(pred, 1), y_)
      accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
tf.summary.scalar('accuracy', accuracy)

merged = tf.summary.merge_all()

if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
train_writer = tf.summary.FileWriter(logdir + "/train")
valid_writer = tf.summary.FileWriter(logdir + "/valid")
saver = tf.train.Saver()

with tf.Session() as sess:
    train_writer.add_graph(sess.graph)
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    sess.run(init_op)

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    epoch = 0
    while epoch < epochs:
        step = 0
        while step * batch_size < training_iters:
            telwd, s1, s2, y = train_reader.get_example_batch(batch_size=batch_size, telescope_model_output_size=telescope_model_output_size)
            if epoch % epochs == epochs -1 and step % max_steps == max_steps - 1:
                run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                run_metadata = tf.RunMetadata()
                summary, _ = sess.run([merged, optimizer],
                                      feed_dict={telwd_: telwd, s1_: s1, s2_: s2, y_: y, keep_prob_: dropout, training_: True},
                              options=run_options,
                              run_metadata=run_metadata)
                train_writer.add_run_metadata(run_metadata, 'step%03d' % int(max_steps*epoch+step))
                train_writer.add_summary(summary, max_steps*epoch+step)
                print('Adding run metadata for step', max_steps*epoch+step) 
            elif step % display_step == 0:
                summary, _ = sess.run([merged, optimizer], feed_dict={telwd_: telwd, s1_: s1, s2_: s2, y_: y, keep_prob_: dropout, training_: True})
                train_writer.add_summary(summary, max_steps*epoch+step)
            else:
                sess.run(optimizer, feed_dict={telwd_: telwd, s1_: s1, s2_: s2, y_: y, keep_prob_: dropout, training_: True})
            step += 1
        acc_sum = 0
        loss_sum = 0
        step = 0
        iters = valid_reader.get_entries()
        max_steps2 = iters / batch_size
        while step * batch_size < iters:
            telwd, s1, s2, y = valid_reader.get_example_batch(batch_size=batch_size, telescope_model_output_size=telescope_model_output_size)
            if step == 0:
                summary, loss, acc = sess.run([merged, cost, accuracy], feed_dict={telwd_: telwd, s1_: s1, s2_: s2, y_: y, keep_prob_: 1., training_: False})
                valid_writer.add_summary(summary, max_steps2*epoch+step)
                acc_sum += acc
                loss_sum += loss
            else:
                loss, acc = sess.run([cost, accuracy], feed_dict={telwd_: telwd, s1_: s1, s2_: s2, y_: y, keep_prob_: 1., training_: False})
                acc_sum += acc
                loss_sum += loss
            step += 1
        print('At epoch %s: Cost %s, Accuracy %s' % (epoch, loss_sum/step, acc_sum/step))
        epoch += 1
  
    print("Optimization Finished!")
#     
#     print("Testing Accuracy:", \
#         sess.run(accuracy, feed_dict={telwd_: telwd, s1_: s1, s2_: s2, y_: y, keep_prob_: 1., training_: False}))

    coord.request_stop()
    coord.join(threads)
    train_writer.close()
    valid_writer.close()
    
    if tf.gfile.Exists(model_weights):
        tf.gfile.DeleteRecursively(model_weights)
    tf.gfile.MakeDirs(model_weights)
    save_path = saver.save(sess, model_weights + "/model.ckpt")
    print("Model saved in file: %s" % save_path)
    sess.close()
