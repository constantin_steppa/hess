from ROOT import TChain
import numpy as np
from root_numpy import hist2array
from random import randint
import tensorflow as tf

class Reader:
    def __init__(self, file_list):
        self.chain = TChain("events")
        for file in file_list:
            self.chain.Add(file)
        print("Read {} events".format(self.chain.GetEntries()))
        self.events = [i for i in range(self.chain.GetEntries())]
        self.npixels = 32 * 18
        self.telescopes = 4
        
    def get_example(self, index=0):
        self.chain.GetEntry(index)
        set1_list = []
        set2_list = []
        label_list = []
        serialised_images = np.array(self.chain.serialised_images)
        serialised_images = serialised_images.astype(np.float32)
        telwd = np.array(self.chain.TelWdata)
        telwd = telwd.astype(np.int32)
        marker = 0
        for tel in telwd:
            if tel == 1:
                set1 = serialised_images[marker:marker + self.npixels]
                set1 = np.reshape(set1, (18, 32, 1))
                marker += self.npixels
                set1_list.append(set1)
                set2 = serialised_images[marker:marker + self.npixels]
                set2 = np.reshape(set2, (18, 32, 1))
                marker += self.npixels
                set2_list.append(set2)
                label = np.array(self.chain.primaryID)
                label = label.astype(np.int32)
                label %= 2
                label_list.append(label)
        return [telwd], set1_list, set2_list, label_list

    def get_random_example(self):
        rnd = randint(0, len(self.events) - 1)
        return self.get_example(self.events.pop(rnd))
    
    def get_example_batch(self, batch_size):
        telwd_list = []
        set1_list = []
        set2_list = []
        label_list = []
        if batch_size > len(self.events):
            batch_size = len(self.events)
        for i in range(batch_size):
            telwd_event, set1_event, set2_event, label_event = self.get_random_example()
            telwd_list += telwd_event
            set1_list += set1_event
            set2_list += set2_event
            label_list += label_event
        telwd_batch = np.stack(telwd_list)
        set1_batch = np.stack(set1_list)
        set2_batch = np.stack(set2_list)
        label_batch = np.stack(label_list)
#         set1_batch = tf.cast(np.stack(set1_list), tf.float32)
#         set2_batch = tf.cast(np.stack(set2_list), tf.float32)
#         telwd_batch = tf.cast(np.stack(telwd_list), tf.int32)
#         label_batch = tf.cast(np.stack(label_list), tf.int64)
        return telwd_batch, set1_batch, set2_batch, label_batch