from __future__ import print_function

import tensorflow as tf
import root_reader
import cnn
# import numpy as np
import math, os

# machine directories
machine = "local"
# machine = "zeuthen"
directory = "/Users/steppa/tf_examples/serialised_root/subsample"
model_path = "/Users/steppa/git/hess/hess1_models/simple_cnn_2"
logdir = model_path +"/log"
model_weights = model_path + "/weights"
if(machine == "zeuthen"):
    directory = "/lustre/fs20/group/hess/user/steppa/tf_examples/root/subsample"
    model_path = "/afs/ifh.de/group/hess/scratch/user/steppa/scripts/hess/hess1_models/simple_cnn_2"
    logdir = model_path + "/log"
    model_weights = model_path + "/weights"

# Parameters
learning_rate = 1e-4
training_iters = 20000
batch_size = 200
display_step = 5
max_steps = training_iters / batch_size

# Network Parameters
n_classes = 2  # gammas and background
dropout = 0.75  # Dropout, probability to keep units

# tf Graph input
with tf.variable_scope('input'):
    s1 = tf.placeholder(tf.float32, [None, 18, 32, 1])
    s2 = tf.placeholder(tf.float32, [None, 18, 32, 1])
    telswd = tf.placeholder(tf.int32, [None, 4])
    label = tf.placeholder(tf.float32, [None, n_classes])
    keep_prob = tf.placeholder(tf.float32)
    training = tf.placeholder(tf.bool)
    
file_list = []
if(machine == "local"):
#     file_list.append(directory + "/proton__0.root")
    for filename in os.listdir(directory):
        file_list += [directory + "/" + filename]
elif(machine == "zeuthen"):
#     file_list.append(directory + "/proton__94.root")
    for filename in os.listdir(directory):
        file_list += [directory + "/" + filename]
reader = root_reader.Reader(file_list)
telwd, s1, s2, y = reader.get_example_batch(batch_size=batch_size)

model = cnn.HESS(tels=4, tel_out_dim=8, n_classes=2, n_res_blocks=2)
pred = model.process(telwd, s1, s2, keep_prob, training)
with tf.variable_scope('cross_entropy'):
    cost = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=pred, labels=y))
    tf.summary.scalar('cross_entropy', cost)
with tf.variable_scope('train'):
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
with tf.variable_scope('accuracy'):
    with tf.variable_scope('correct_prediction'):
      correct_pred = tf.equal(tf.argmax(pred, 1), y)
      accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
tf.summary.scalar('accuracy', accuracy)

merged = tf.summary.merge_all()

if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
train_writer = tf.summary.FileWriter(logdir + "/train")
test_writer = tf.summary.FileWriter(logdir + "/test")
saver = tf.train.Saver()

with tf.Session() as sess:
    train_writer.add_graph(sess.graph)
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    sess.run(init_op)

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    step = 1
    
    while step * batch_size < training_iters:
        sess.run(optimizer, feed_dict={keep_prob: dropout, training: True})
        if step % display_step == 0:
            summary, loss, acc, label_batch = sess.run([merged, cost, accuracy, y], feed_dict={keep_prob: 1., training: False})
            test_writer.add_summary(summary, step)
            label_list = tf.unstack(label_batch)
            protons = 0.
            for label in label_list:
                protons += label.eval()
            protons = protons / batch_size
            print('At step %s: Cost %s, Accuracy %s with %s protons' % (step, loss, acc, protons))
        else:
            if step % max_steps == max_steps - 2:
                run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                run_metadata = tf.RunMetadata()
                summary, _ = sess.run([merged, optimizer],
                                      feed_dict={keep_prob: dropout, training: True},
                              options=run_options,
                              run_metadata=run_metadata)
                train_writer.add_run_metadata(run_metadata, 'step%03d' % step)
                train_writer.add_summary(summary, step)
                print('Adding run metadata for', step) 
            else:
                summary, _ = sess.run([merged, optimizer], feed_dict={keep_prob: dropout, training: True})
                train_writer.add_summary(summary, step)
        step += 1
  
    print("Optimization Finished!")
    
    print("Testing Accuracy:", \
        sess.run(accuracy, feed_dict={keep_prob: 1., training: False}))

    coord.request_stop()
    coord.join(threads)
    train_writer.close()
    test_writer.close()
    
    if tf.gfile.Exists(model_weights):
        tf.gfile.DeleteRecursively(model_weights)
    tf.gfile.MakeDirs(model_weights)
    save_path = saver.save(sess, model_weights + "/model.ckpt")
    print("Model saved in file: %s" % save_path)
    sess.close()
